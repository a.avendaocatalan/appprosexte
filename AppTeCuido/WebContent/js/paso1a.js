var verRut=false;
var verSerie = false;
var verNumero=false;
var verMail=false;
var verRecapcha=false;
    	
$( document ).ready(function() {
    
    $("#rut").blur(function() {
    	 var element = $(this).val().trim();
    	 var mystring = element.replace(/\./g,'');
    	 var iscorrect = validateRut(mystring);
    	 if(iscorrect){
    		 $(this).parent().addClass("iscorrect");
    		 $(this).parent().removeClass("error-input");
    		 verRut=true;
    		 habilitaBoton()
    	 }else{
    		 $(this).parent().addClass("error-input");
    		 $(this).parent().removeClass("iscorrect");
    		 verRut=false;
    	 }
    	 $(this).val(formateaRut(element));
    });
    
    
    
    $("#nSerie").blur(function() {
    	var element = $(this).val().trim();
    	if(element != ''){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input");
    		verSerie=true;
    		habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input");
    		 $(this).parent().removeClass("iscorrect");
    		 verSerie=false;
    	}
    });
    
    $("#telefono").keydown(function(event) {
	   if(event.shiftKey)
	   {
	        event.preventDefault();
	   }

	   if (event.keyCode == 46 || event.keyCode == 8)    {
	   }
	   else {
	        if (event.keyCode < 95) {
	          if (event.keyCode < 48 || event.keyCode > 57) {
	                event.preventDefault();
	          }
	        } 
	        else {
	              if (event.keyCode < 96 || event.keyCode > 105) {
	                  event.preventDefault();
	              }
	        }
	      }
	   });
	
    
    $("#telefono").blur(function() {
    	var element = $(this).val().trim();
    	if(element != '' && element.length == 9){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input");
    		verNumero=true;
    		habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input");
    		$(this).parent().removeClass("iscorrect");
    		verNumero=false; 
    	}
    });
    
    $("#email").blur(function() {
    	var element = $(this).val().trim();
    	var ismail = isEmail(element);
    	if(ismail){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input");
    		verMail=true;
    		habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input");
    		 $(this).parent().removeClass("iscorrect");
    		 verMail=false;
    	}
    });
    
    $("#btnContinuar").click(function(e){
    	e.preventDefault();
    	if(verificaForm()){
    		$("#formPaso1A").submit();
    	}
    });
    
});
	    
function habilitaBoton(){
	if(verRut && verSerie && verNumero&&verRecapcha&&verMail){
		$("#btnContinuar").removeClass("disabled");
		
	}
}

function verificaForm(){
	if(!verRut){
		$("#rut").parent().addClass("error-input");
		$("#rut").parent().removeClass("iscorrect");
	}
	if(!verSerie){
		$("#nSerie").parent().addClass("error-input");
		$("#nSerie").parent().removeClass("iscorrect");
	}
	if(!verNumero){
		$("#telefono").parent().addClass("error-input");
		$("#telefono").parent().removeClass("iscorrect");
	}
	if(!verMail){
		$("#email").parent().addClass("error-input");
		$("#email").parent().removeClass("iscorrect");
	}
	
	if(verRut && verSerie && verNumero&&verRecapcha&&verMail){
		return true;
	}else{
		return false;
	}
}

function formateaRut(rut) {
	
    var actual = rut.replace(/^0+/, "");
    if (actual != '' && actual.length > 1) {
        var sinPuntos = actual.replace(/\./g, "");
        var actualLimpio = sinPuntos.replace(/-/g, "");
        var inicio = actualLimpio.substring(0, actualLimpio.length - 1);
        var rutPuntos = "";
        var i = 0;
        var j = 1;
        for (i = inicio.length - 1; i >= 0; i--) {
            var letra = inicio.charAt(i);
            rutPuntos = letra + rutPuntos;
            if (j % 3 == 0 && j <= inicio.length - 1) {
                rutPuntos = "." + rutPuntos;
            }
            j++;
        }
        var dv = actualLimpio.substring(actualLimpio.length - 1);
        rutPuntos = rutPuntos + "-" + dv;
    }
    return rutPuntos;
}

function verificaRecapcha(){
	if( grecaptcha.getResponse().length != 0){
		verRecapcha=true;
		habilitaBoton()
	}else{
		verRecapcha=false;
	}
}


function validateRut(_value) {
    if(typeof _value !== 'string') return false;
    var t = parseInt(_value.slice(0,-1), 10), m = 0, s = 1;
    while(t > 0) {
      s = (s + t%10 * (9 - m++%6)) % 11;
      t = Math.floor(t / 10);
    }
    var v = (s > 0) ? (s-1)+'' : 'K';
    return (v === _value.slice(-1));
}

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}