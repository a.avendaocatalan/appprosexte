var verRegion =false;
var verComuna = false;
var verCalle = false;
var verNumero = false;


$( document ).ready(function() {
	
	$("#btnContinuar").click(function(e){
		e.preventDefault();
		if(verificaForm()){
			$("#formPaso3").submit();
		}
	});
	
	$("#calle").blur(function() {
    	var element = $(this).val().trim();
    	if(element != ''){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input");
    		verCalle=true;
    		habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input");
    		 $(this).parent().removeClass("iscorrect");
    		 verCalle=false;
    	}
    });
	
	$("#numCalle").blur(function() {
    	var element = $(this).val().trim();
    	if(element != ''){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input");
    		verNumero=true;
    		habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input");
    		 $(this).parent().removeClass("iscorrect");
    		 verNumero=false;
    	}
    });
	
	$("#selRegion").change(function() {
		var reg = $(this).val();
		ajaxComunas(reg);
    	if(reg != ''){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input");
    		verRegion=true;
    		habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input");
    		$(this).parent().removeClass("iscorrect");
    		verRegion=false; 
    	}
    });
	
	
	$("#selComuna").change(function() {
		var com = $(this).val();
		ajaxmsg(com);
    	if(com != ''){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input");
    		verComuna=true;
    		habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input");
    		$(this).parent().removeClass("iscorrect");
    		verComuna=false; 
    	}
    });
});

function ajaxComunas(region){
	var reg1 = region.replace(" ","_");
	reg1 = reg1.replace(" ","_");
	reg1 = normalize(reg1)
	verComuna=false
	if(region != ""){
		
		$.ajax({
			url:"msgComAjax",
			async:true,
			data:"region="+reg1,
			success: function(data){
				var comunas = data.split(",");
				var listCom = "<option value=\"\">Seleccionar</option>";
				for(var i=0;i<comunas.length;i++){
					listCom+="<option value=\""+comunas[i]+"\">"+comunas[i]+"</option>"
				}
				$("#selComuna" ).prop( "disabled", false );
				$("#selComuna").html(listCom);
			}
		});
	}else{
		$("#selComuna" ).prop( "disabled", true );
		
	}
}

var normalize = (function() {
	  var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç", 
	      to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
	      mapping = {};
	 
	  for(var i = 0, j = from.length; i < j; i++ )
	      mapping[ from.charAt( i ) ] = to.charAt( i );
	 
	  return function( str ) {
	      var ret = [];
	      for( var i = 0, j = str.length; i < j; i++ ) {
	          var c = str.charAt( i );
	          if( mapping.hasOwnProperty( str.charAt( i ) ) )
	              ret.push( mapping[ c ] );
	          else
	              ret.push( c );
	      }      
	      return ret.join( '' );
	  }
	 
	})();

function ajaxmsg(comuna){
	$.ajax({
		url:"msgComAjax",
		async:true,
		data:"comuna="+comuna,
		success: function(data){
			$("#msgAjax").html(data);
		}
	});
}

function habilitaBoton(){
	if(verRegion&&verComuna&&verCalle&&verNumero){
		$("#btnContinuar").removeClass("disabled");		
	}
}

function verificaForm(){
	if(!verRegion){
		$("#selRegion").parent().addClass("error-input");
		$("#selRegion").parent().removeClass("iscorrect");
	}
	if(!verComuna){
		$("#selComuna").parent().addClass("error-input");
		$("#selComuna").parent().removeClass("iscorrect");
	}
	if(!verCalle){
		$("#calle").parent().addClass("error-input");
		$("#calle").parent().removeClass("iscorrect");
	}
	if(!verNumero){
		$("#numCalle").parent().addClass("error-input");
		$("#numCalle").parent().removeClass("iscorrect");
	}
	
	
	if(verRegion&&verComuna&&verCalle&&verNumero){
		$("#btnContinuar").removeClass("disabled");
		return true;
	}else{
		$("#btnContinuar").addClass("disabled");
		return false;
	}
}