var verCap=false;
var verImei=false;
var verCond = false;

$( document ).ready(function() {
	
	$("#btnContinuar").click(function(e){
		e.preventDefault();
		if(verificaForm()){
			$("#formPaso2B").submit();
		}
	});
	
	$("#codCap").blur(function() {
    	var element = $(this).val().trim();
    	if(element != ''){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input");
    		verCap=true;
    		habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input");
    		 $(this).parent().removeClass("iscorrect");
    		 verNombre=false;
    	}
    });
	
	$("#imei").blur(function() {
    	var element = $(this).val().trim();
    	if(element != ''){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input");
    		verImei=true;
    		habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input");
    		 $(this).parent().removeClass("iscorrect");
    		 verNombre=false;
    	}
    });
	
	$("#checkPorta").click(function(e){
		verCond = $(this).is(':checked');
		habilitaBoton()
	});
});

function verificaForm(){
	if(!verCap){
		$("#codCap").parent().addClass("error-input");
		$("#codCap").parent().removeClass("iscorrect");
	}
	if(!verImei){
		$("#imei").parent().addClass("error-input");
		$("#imei").parent().removeClass("iscorrect");
	}
	
	
	if(verCap&&verImei&&verCond){
		$("#btnContinuar").removeClass("disabled");
		return true;
	}else{
		$("#btnContinuar").addClass("disabled");
		return false;
	}
}
function habilitaBoton(){
	
	if(verCap&&verImei&&verCond){
		$("#btnContinuar").removeClass("disabled");		
	}else{
		$("#btnContinuar").addClass("disabled");
	}
}