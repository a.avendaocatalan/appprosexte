var verNombre=false;
var verApellidos = false;
var verNumPortar=false;
var verCompania=false;
var verMercado=false;

$( document ).ready(function() {
	
	if($("#nombreCliente").val() != ''){
		verNombre=true;
	}
	
	if($("#apellidosCliente").val() != ''){
		verApellidos=true;
	}

	$("#nombreCliente").blur(function() {
    	var element = $(this).val().trim();
    	if(element != ''){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input");
    		verNombre=true;
    		habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input");
    		 $(this).parent().removeClass("iscorrect");
    		 verNombre=false;
    	}
    });
	
	$("#apellidosCliente").blur(function() {
    	var element = $(this).val().trim();
    	if(element != ''){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input");
    		verApellidos=true;
    		habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input");
    		 $(this).parent().removeClass("iscorrect");
    		 verApellidos=false;
    	}
    });
	
	$("#numeroPortar").keydown(function(event) {
		   if(event.shiftKey)
		   {
		        event.preventDefault();
		   }

		   if (event.keyCode == 46 || event.keyCode == 8)    {
		   }
		   else {
		        if (event.keyCode < 95) {
		          if (event.keyCode < 48 || event.keyCode > 57) {
		                event.preventDefault();
		          }
		        } 
		        else {
		              if (event.keyCode < 96 || event.keyCode > 105) {
		                  event.preventDefault();
		              }
		        }
		      }
		   });
	
	$("#numeroPortar").blur(function() {
    	var element = $(this).val().trim();
    	if(element != '' && element.length == 9){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input");
    		verNumPortar=true;
    		habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input");
    		$(this).parent().removeClass("iscorrect");
    		verNumPortar=false; 
    	}
    });
	
	$("#companiaMovil").change(function() {
		var comp = $(this).val();
    	if(comp != ''){
    		$(this).parent().addClass("iscorrect");
    		$(this).parent().removeClass("error-input");
    		verCompania=true;
    		habilitaBoton()
    	}else{
    		$(this).parent().addClass("error-input");
    		$(this).parent().removeClass("iscorrect");
    		verCompania=false; 
    	}
    });
	
	$("#test1").click(function(e){
		$("#test1").parents('.form-input').addClass("iscorrect");
		$("#test1").parents('.form-input').removeClass("error-input");
		verMercado=true;
		habilitaBoton()
	});
	
	$("#test2").click(function(e){
		$("#test1").parents('.form-input').addClass("iscorrect");
		$("#test1").parents('.form-input').removeClass("error-input");
		verMercado=true;
		habilitaBoton()
	});
	
	$("#btnContinuar").click(function(e){
    	e.preventDefault();
    	if(verificaForm()){
    		$("#formPaso2A").submit();
    	}
    });
	
	
	
});

function habilitaBoton(){
	
	if(verNombre && verApellidos && verNumPortar&&verCompania&&verMercado){
		$("#btnContinuar").removeClass("disabled");		
	}
}

function verificaForm(){
	if(!verNombre){
		$("#nombreCliente").parent().addClass("error-input");
		$("#nombreCliente").parent().removeClass("iscorrect");
	}
	if(!verApellidos){
		$("#apellidosCliente").parent().addClass("error-input");
		$("#apellidosCliente").parent().removeClass("iscorrect");
	}
	if(!verNumPortar){
		$("#numeroPortar").parent().addClass("error-input");
		$("#numeroPortar").parent().removeClass("iscorrect");
	}
	if(!verCompania){
		$("#companiaMovil").parent().addClass("error-input");
		$("#companiaMovil").parent().removeClass("iscorrect");
	}
	if(!verMercado){
		$("#test1").parents('.form-input').addClass("error-input");
		$("#test1").parents('.form-input').removeClass("iscorrect");
	}
	
	if(verNombre && verApellidos && verNumPortar&&verCompania&&verMercado){
		$("#btnContinuar").removeClass("disabled");
		return true;
	}else{
		$("#btnContinuar").addClass("disabled");
		return false;
	}
}