package com.esa.ponline.appmobile.ws;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;

import org.apache.log4j.Logger;

import com.epcs.clientes.evaluacion.EvaluacionComercialNecService;
import com.epcs.integracionit.flujotrabajo.EnviarMailService;
import com.esa.cliente.contacto.sga.TicketSGAService;
import com.esa.crm.portabilidad.t.obtenercompaniamovil.ValidaMsisdnService;
import com.esa.marketsales.salesmassive.n.enviarcapinternosuscripcion.EnviarCapInternoSuscripcion;
import com.esa.marketsales.salesmassive.n.validardatosclientesimaut.ValidarDatosClienteSIMAUT;
import com.esa.marketsales.salesmassive.n.validardatosportabilidadsimaut.ValidarDatosPortabilidadSIMAUT;
import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.imp.ServicesImplements;

import cl.iecisa.gateway.ws.integrator.services.ssgp_consultafactportacionsync.SSGPConsultaFactPortacionSyncService;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultatitulardeudasync.SSGPConsultaTitularDeudaSyncService;
import cl.iecisa.gateway.ws.integrator.services.ssgp_solicitacapasync.SSGPSolicitaCAPAsyncService;

public class EntelServices {
	private static final Logger LOGGER 	= Logger.getLogger(ServicesImplements.class);
	public static final boolean REUSE             = true;
	private static EvaluacionComercialNecService evaluacionComercialNecService         = null;
	private static ValidarDatosClienteSIMAUT validaDatosClienteSIMAUT                  = null;
	private static SSGPConsultaTitularDeudaSyncService consultaTitularDeudaSyncService = null;
	private static ValidaMsisdnService validaCompaniaService                           = null;
	private static SSGPSolicitaCAPAsyncService solicitaCap                             = null;
	private static EnviarCapInternoSuscripcion generaCapSuscripcion                    = null;
	private static ValidarDatosPortabilidadSIMAUT validarDatosPortabilidad             = null;
	private static SSGPConsultaFactPortacionSyncService consultaFactPortacion          = null;
	private static EnviarMailService respuestaEmailInstanceService  				   = null;
	private static TicketSGAService ticketSGAService                                   = null;
	
	
	
	public static ValidarDatosPortabilidadSIMAUT validarDatosPortabilidad() {
		if (validarDatosPortabilidad              != null && REUSE) {
			return validarDatosPortabilidad;
		}		
		try {
			String urlWS = Config.getCOServices("validarDatosPortabilidadEndPoint");
			LOGGER.info("urlWS: " + urlWS);
			validarDatosPortabilidad = new ValidarDatosPortabilidadSIMAUT(new URL(urlWS), new QName("http://www.esa.com/MarketSales/SalesMassive/N/validarDatosPortabilidadSIMAUT", "validarDatosPortabilidadSIMAUT"));
		} catch (MalformedURLException e) {
			LOGGER.error("No es posible crear la llamada al servicio ValidarDatosPortabilidadSIMAUT: " + e.getMessage());
			LOGGER.error( e );
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + ValidarDatosPortabilidadSIMAUT.class, e);
			try {
				throw new EntelServicesLocatorException(e.getMessage());
			} catch (EntelServicesLocatorException e1) {
				//LOGGER.error("No es posible crear la llamada al servicio ValidarDatosPortabilidadSIMAUT: " + e.getMessage());
				e1.printStackTrace();
			}
		}
		return validarDatosPortabilidad;	
	}
	public static EnviarCapInternoSuscripcion generaCapSuscripcion() {
		if (generaCapSuscripcion                     != null && REUSE) {
			return generaCapSuscripcion;
		}		
		try {
			String urlWS = Config.getCOServices("generaCapSuscripcionEndPoint");
			LOGGER.info("urlWS: " + urlWS);
			generaCapSuscripcion = new EnviarCapInternoSuscripcion(new URL(urlWS), new QName("http://www.esa.com/MarketSales/SalesMassive/N/enviarCapInternoSuscripcion", "enviarCapInternoSuscripcion"));
		} catch (MalformedURLException e) {
			LOGGER.error("No es posible crear la llamada al servicio EnviarCapInternoSuscripcion: " + e.getMessage());
			LOGGER.error( e );
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + EnviarCapInternoSuscripcion.class, e);
			try {
				throw new EntelServicesLocatorException(e.getMessage());
			} catch (EntelServicesLocatorException e1) {
				//LOGGER.error("No es posible crear la llamada al servicio EnviarCapInternoSuscripcion: " + e.getMessage());
				e1.printStackTrace();
			}
		}
		return generaCapSuscripcion;	
	}
	public static SSGPSolicitaCAPAsyncService generaCap() {
		if (solicitaCap                              != null && REUSE) {
			return solicitaCap;
		}
		
		try {
			String urlWS = Config.getCOServices("generaCapEndPoint");
			LOGGER.info("urlWS: " + urlWS);
			solicitaCap = new SSGPSolicitaCAPAsyncService(new URL(urlWS), new QName("http://SSGP_SolicitaCAPAsync.services.integrator.ws.gateway.iecisa.cl/", "SSGP_SolicitaCAPAsyncService"));
		} catch (MalformedURLException e) {
			LOGGER.error("No es posible crear la llamada al servicio SSGPSolicitaCAPAsyncService: " + e.getMessage());
			LOGGER.error( e );
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + SSGPSolicitaCAPAsyncService.class, e);
			try {
				throw new EntelServicesLocatorException(e.getMessage());
			} catch (EntelServicesLocatorException e1) {
				//LOGGER.error("No es posible crear la llamada al servicio SSGPSolicitaCAPAsyncService: " + e.getMessage());
				e1.printStackTrace();
			}
		}
		return solicitaCap;	
	}

	public static ValidaMsisdnService validaCompania() throws EntelServicesLocatorException {
		if (validaCompaniaService != null && REUSE) {
			return validaCompaniaService;
		}

		try {
			String urlWS = Config.getCOServices("obtenerCompaniaEndPoint");
			
			validaCompaniaService = new ValidaMsisdnService(
					new URL(urlWS),
					new QName("http://www.esa.com/CRM/Portabilidad/T/ObtenerCompaniaMovil", "validaMsisdnService"));
		} catch (MalformedURLException e) {
			LOGGER.error("No es posible crear la llamada al servicio ValidaMsisdnService: " + e.getMessage());
			LOGGER.error( e );
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + ValidaMsisdnService.class, e);
			try {
				throw new EntelServicesLocatorException(e.getMessage());
			} catch (EntelServicesLocatorException e1) {
				LOGGER.error("No es posible crear la llamada al servicio ValidaMsisdnService: " + e.getMessage());
				e1.printStackTrace();
			}
		}
		return validaCompaniaService;
	}
	
	public static SSGPConsultaTitularDeudaSyncService getConsultaTitularDeudaSyncService() {
		if (consultaTitularDeudaSyncService != null && REUSE) {
			return consultaTitularDeudaSyncService;
		}
		
		try {
			String urlWS = Config.getCOServices("consultaTitularDeudaSyncServiceEndPoint");
			LOGGER.info("urlWS: " + urlWS);
			consultaTitularDeudaSyncService = new SSGPConsultaTitularDeudaSyncService(
					new URL(urlWS), 
					new QName("http://SSGP_ConsultaTitularDeudaSync.services.integrator.ws.gateway.iecisa.cl/", 
					"SSGP_ConsultaTitularDeudaSyncService")
			);
		} catch (MalformedURLException e) {
			LOGGER.error("No es posible crear la llamada al servicio SSGPConsultaTitularDeudaSyncService: " + e.getMessage());
			LOGGER.error( e );
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + SSGPConsultaTitularDeudaSyncService.class, e);
			try {
				throw new EntelServicesLocatorException(e.getMessage());
			} catch (EntelServicesLocatorException e1) {
				LOGGER.error("No es posible crear la llamada al servicio SSGPConsultaTitularDeudaSyncService: " + e.getMessage());
				e1.printStackTrace();
			}
		}
		
		return consultaTitularDeudaSyncService;	
	}
	
	public static ValidarDatosClienteSIMAUT getValidaDatosClienteSIMAUT() {
		if (validaDatosClienteSIMAUT != null && REUSE) {
			return validaDatosClienteSIMAUT;
		}		
		try {
			String urlWS = Config.getCOServices("validaDatosClienteSIMAUTEndPoint");
			LOGGER.info("urlWS: " + urlWS);
			validaDatosClienteSIMAUT = new ValidarDatosClienteSIMAUT (new URL(urlWS), new QName("http://www.esa.com/MarketSales/SalesMassive/N/validarDatosClienteSIMAUT", "validarDatosClienteSIMAUT"));
		} catch (MalformedURLException e) {
			LOGGER.error("No es posible crear la llamada al servicio ValidaDatosClienteSIMAUT: " + e.getMessage());
			LOGGER.error( e );
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + EnviarCapInternoSuscripcion.class, e);
			try {
				throw new EntelServicesLocatorException(e.getMessage());
			} catch (EntelServicesLocatorException e1) {
				LOGGER.error("No es posible crear la llamada al servicio ValidaDatosClienteSIMAUT: " + e.getMessage());
				e1.printStackTrace();
			}
		}
		return validaDatosClienteSIMAUT;	
	}
	public static EvaluacionComercialNecService getEvaluacionComercial() throws EntelServicesLocatorException {
		if (evaluacionComercialNecService != null && REUSE) {
			return evaluacionComercialNecService;
		}
		
		try {
			String urlWS = Config.getCOServices("evaluacionComercialEndPoint");
			LOGGER.info("urlWS: " + urlWS);
			evaluacionComercialNecService = new EvaluacionComercialNecService(new URL(urlWS), new QName("http://www.epcs.com/clientes/evaluacion", "evaluacionComercialNecService"));
		} catch (MalformedURLException e) {
			LOGGER.error("No es posible crear la llamada al servicio EvaluacionComercialNecService: " + e.getMessage());
			LOGGER.error( e );
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + EvaluacionComercialNecService.class, e);
			try {
				throw new EntelServicesLocatorException(e.getMessage());
			} catch (EntelServicesLocatorException e1) {
				LOGGER.error("No es posible crear la llamada al servicio EvaluacionComercialNecService: " + e.getMessage());
				e1.printStackTrace();
			}
		}
		return evaluacionComercialNecService;		
	}
	public static SSGPConsultaFactPortacionSyncService getConsultaFactPortacion() {
		if (consultaFactPortacion != null && REUSE) {
			return consultaFactPortacion;
		}		
		
		try {
			String urlWS = Config.getCOServices("consultaFactPortacionEndPoint");
			LOGGER.info("urlWS: " + urlWS);
			consultaFactPortacion = new SSGPConsultaFactPortacionSyncService (new URL(urlWS), new QName("http://SSGP_ConsultaFactPortacionSync.services.integrator.ws.gateway.iecisa.cl/", "SSGP_ConsultaFactPortacionSyncService"));
		} catch (MalformedURLException e) {
			LOGGER.error("No es posible crear la llamada al servicio ConsultaFactPortacion: " + e.getMessage());
			LOGGER.error( e );
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + EnviarCapInternoSuscripcion.class, e);
			try {
				throw new EntelServicesLocatorException(e.getMessage());
			} catch (EntelServicesLocatorException e1) {
				LOGGER.error("No es posible crear la llamada al servicio ConsultaFactPortacion: " + e.getMessage());
				e1.printStackTrace();
			}
		}
		
		return consultaFactPortacion;	
	}
	public static EnviarMailService getRespuestaEmailInstanceService() throws EntelServicesLocatorException {
		if (respuestaEmailInstanceService != null && REUSE) {
			return respuestaEmailInstanceService;
		}
		try {

			respuestaEmailInstanceService = new EnviarMailService(new URL(Config.getCOServices("EmailEndPoint")),
			//respuestaEmailInstanceService = new EnviarMailService(new URL("http://esb.entel.cl/IntegracionIT/enviomail/Proxies/enviamailPS?wsdl"),
			new QName("http://www.epcs.com/integracionit/flujotrabajo", "EnviarMailService"));

		} catch (MalformedURLException e) {
			LOGGER.error("No es posible crear la llamada al servicio EnviarMailService: " + e.getMessage());
			LOGGER.error( e );
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + EnviarMailService.class, e);
			try {
				throw new EntelServicesLocatorException(e.getMessage());
			} catch (EntelServicesLocatorException e1) {
				LOGGER.error("No es posible crear la llamada al servicio EnviarMailService: " + e.getMessage());
				
			}
		}

		return respuestaEmailInstanceService;
	}
	public static TicketSGAService getTicketSGAService() throws EntelServicesLocatorException {
		
		if (ticketSGAService                                    != null && REUSE) {
			return ticketSGAService;
		}
		String urlWS = Config.getCOServices("generaTicketSGAEndPoint");
		try {
			ticketSGAService = new TicketSGAService(					
				new URL(urlWS),
				new QName("http://www.esa.com/cliente/contacto/sga/", "TicketSGAService")
			);

			// } catch (MalformedURLException e) {
			// LOGGER.error("No es posible crear la llamada al servicio
			// TicketSGAService: " + e.getMessage());
			// LOGGER.error( e );
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + TicketSGAService.class, e);
			try {
				throw new EntelServicesLocatorException(e.getMessage());
			} catch (EntelServicesLocatorException e1) {
				LOGGER.error("No es posible crear la llamada al servicio TicketSGAService: " + e.getMessage());
				e1.printStackTrace();
			}
		}

		return ticketSGAService;
	}
}
