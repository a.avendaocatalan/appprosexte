package com.esa.ponline.appmobile.ws;

import com.esa.ponline.appmobile.imp.ServicesImplements;
import com.esa.ponline.appmobile.interfaces.IServicesInterfaces;

public class FactoryWSDAO {
	private static IServicesInterfaces servicesI;
	
	public static IServicesInterfaces getServices() {
		if(servicesI == null){
			servicesI = new ServicesImplements();
			return servicesI;
		}else{
			return servicesI;
		}
		
	}
}
