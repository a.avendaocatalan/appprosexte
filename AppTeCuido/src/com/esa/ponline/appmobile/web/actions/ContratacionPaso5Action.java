package com.esa.ponline.appmobile.web.actions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.email.MailDatosHelper;
import com.esa.ponline.appmobile.email.MailType;
import com.esa.ponline.appmobile.utils.FormatoMovilUtil;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.DataEmailVO;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.esa.ponline.appmobile.web.bean.ResTicketSGAVO;
import com.esa.ponline.appmobile.web.bean.TicketSGAVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cl.esa.ponline.appmobile.delegate.DelegateServices;

public class ContratacionPaso5Action extends ActionSupport  {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7264720493176902590L;
	private static final Logger LOGGER = Logger.getLogger(ContratacionAction.class);
	private static final Logger	logMetricas	= Logger.getLogger("Metricas");
	private String paso5Page 			= "ContratacionPaso5";
	private String paso5ErrorPage 		= "ContratacionPaso5Error";
	
	private Plan planCO;
	private Cliente clienteCO;
	
	private DelegateServices delegateServices = new DelegateServices();
	public String execute(){
		String response="";
		HttpServletRequest req = ServletActionContext.getRequest();
		clienteCO = (Cliente) getSession().get("Cliente");
		planCO = (Plan) getSession().get("Plan");
		response = contratacionPaso5(clienteCO, planCO);
		
		return response;
	}
	
	private String contratacionPaso5(Cliente clienteCO, Plan planCO) {
		String resp=paso5ErrorPage;
		String requerimiento="";
		//GENERO TICKET
		if(Boolean.valueOf(Config.getAppProperty("switchPRueba"))){
			requerimiento=	"|Solicitud:Portabilidad|Nombre: PRUEBA PRUEBA" + 
				 	"|RUT: PRUEBA|NombrePlan: PRUEBA"+
				 	"|DatosPlan: "+ planCO.getDatosPlan() +"GB|MinutosPlan: "+ planCO.getMinutosPlan()+
				 	"|CargoFijo: "+ planCO.getCargoPlan()+
				 	"|Numero a portar: PRUEBA"+
				 	"|Flujo: Contratacion Online Movil"+
				 	"|PortalOrigen: P�blico Mobile|";
		}else{
		requerimiento=	"|Solicitud:Portabilidad|Nombre:"+ clienteCO.getNombre() + " " + clienteCO.getApellidos()+
							 	"|RUT: "+ clienteCO.getRut() +
							 	"|NumSerie: "+ clienteCO.getnSerie()+
							 	"|NombrePlan: "+ planCO.getNombrePlan() +
							 	"|CompaniaOrigen: "+ clienteCO.getCompaniaOrigen()+
							 	"|DatosPlan: "+ planCO.getDatosPlan() +"GB|MinutosPlan: "+ planCO.getMinutosPlan()+
							 	"|CargoFijo: "+ planCO.getCargoPlan()+
							 	"|Numero a portar: "+ clienteCO.getNumeroPorta()+
							 	"|Flujo: Contratacion Online Movil"+
							 	"|Motivo: Nuevas Lineas Portabilidad"+
							 	"|PortalOrigen: P�blico Mobile|"+
							 	"|Numero Negocio: "+ clienteCO.getNumeroNegocio()+
							 	"|DireccionDespacho:"+ clienteCO.getCalleDespacho() + " #"+ clienteCO.getNumeroDespacho() +" "+ clienteCO.getComunaDespacho() +", "+ clienteCO.getRegionDespacho();
		}
		
		LOGGER.info("REQUERIMIENTO:" +requerimiento);
		if(generarTicket(clienteCO,requerimiento)){
			resp=paso5Page;
		}
		//ENVIO EMAIL
		String respuestaEmail = "";				
		try {
			respuestaEmail = envioMail("no-responder@entel.cl", "Bienvenido a Entel", clienteCO.getEmail());			
		} catch (Exception e) {
			LOGGER.error("Error al enviar el Email");
			e.printStackTrace();
		}
		
		if(respuestaEmail.equals("NoOk")){
			LOGGER.error("Env�o de correo fallido, revisar servicio o plantilla de correo");
		}
		if(!resp.equals(paso5ErrorPage)){
			logMetricas.info("|PORTAL|ContratacionOnline|TIPO|INFO|USUARIO|"+clienteCO.getTelefono()+"|RUT|"+  clienteCO.getRut() + "|FLUJO|ContratacionOnline|ETAPA|Paso5|NUMEROSERIE|"+clienteCO.getnSerie()+"|NUMERONEGOCIO|"+ clienteCO.getNumeroNegocio() +"|CAPTCHA|true|NUMEROAPORTAR|"+clienteCO.getNumeroPorta() +"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|RESPUESTAEMAIL|"+respuestaEmail+"|CODIGOCAP||IMEI||GENERATICKET|true|");
		}else{
			logMetricas.error("|PORTAL|ContratacionOnline|TIPO|ERROR|USUARIO|"+clienteCO.getTelefono()+"|RUT|"+  clienteCO.getRut() + "|FLUJO|ContratacionOnline|ETAPA|Paso5|NUMEROSERIE|"+clienteCO.getnSerie()+"|NUMERONEGOCIO|"+ clienteCO.getNumeroNegocio() +"|CAPTCHA|true|NUMEROAPORTAR|"+clienteCO.getNumeroPorta() +"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|RESPUESTAEMAIL|"+respuestaEmail+"|CODIGOCAP||IMEI||GENERATICKET|false|");

		}
		return resp;
	}
	
	private boolean generarTicket(Cliente clienteCO, String requerimiento){
		boolean resp=false;
		TicketSGAVO ticket = new TicketSGAVO();
		ResTicketSGAVO resTicket = new ResTicketSGAVO();
		if(clienteCO.getNombre()!=null && clienteCO.getRut()!=null && clienteCO.getNumeroPorta() != null ){
			ticket.setSolicitud("R");
			ticket.setValor(clienteCO.getRut().replace("-", ""));
			ticket.setPlataforma("Portal MiEntel");
			ticket.setMundo("Ventas Campanas Beneficios");
			ticket.setSuboperacion("Venta Movil Ecommerce");
			ticket.setTipo("Solicitud");
			ticket.setMotivo("Nuevas Lineas Portabilidad");
			ticket.setWorkflow("1");
			ticket.setCelularReclamado(FormatoMovilUtil.obtieneFormatoMovilPorLargo(clienteCO.getNumeroPorta()));
			ticket.setNroSolicitud("");
			ticket.setRequerimientoCliente(requerimiento);
		}
		try {
			if(null != ticket){
				resTicket = delegateServices.ticketSGA(ticket);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
		if(resTicket.getCodigo().equals("0000")){
			resp = true;
		}
		LOGGER.error("TicketSGA cod. "+resTicket.getCodigo()+" Mensaje: "+ resTicket.getMensaje());
		return resp;
		
	}
	
	private String envioMail(String remitente,String asunto, String to) throws Exception {
	 	String respuesta = "NoOk";
		LOGGER.info("Ingresa a cuerpoEmailConfirmacion para el envio de correo de: remitente" + remitente + ", asunto "+asunto+", to "+ to);

		MailType objMailType = MailDatosHelper.getDatosMail();
		String strCuerpoEMail = MailDatosHelper.cuerpoEmailConfirmacion(objMailType);
		List<DataEmailVO> adjuntos = new ArrayList<DataEmailVO>();
		respuesta = delegateServices.enviaEmail(to, remitente, asunto,strCuerpoEMail, adjuntos );
		
		return respuesta;
		
	}
	
	private Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}
}
