package com.esa.ponline.appmobile.web.actions;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.email.MailDatosHelper;
import com.esa.ponline.appmobile.email.MailType;
import com.esa.ponline.appmobile.utils.FormatoMovilUtil;
import com.esa.ponline.appmobile.utils.PruebaUtil;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.esa.ponline.appmobile.web.bean.TicketSGAVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cl.esa.ponline.appmobile.delegate.DelegateServices;

public class ContratacionPaso4Action extends ActionSupport  {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7264720493176902590L;
	private static final Logger LOGGER = Logger.getLogger(ContratacionAction.class);
	private static final Logger	logMetricas	= Logger.getLogger("Metricas");
	private String paso4Page 			= "ContratacionPaso4";
	private String paso4ErrorPage 		= "ContratacionPaso4Error";
	
	private Cliente clienteCO;
	
	
	public String execute(){
		if(Boolean.valueOf(Config.getAppProperty("switchPRueba"))){
			PruebaUtil.creaDatosPrueba();
		}
		String response="";
		HttpServletRequest req = ServletActionContext.getRequest();
		clienteCO = (Cliente) getSession().get("Cliente");
		String comunaDespacho			= req.getParameter("selComuna").toString();
		String regionDespacho			= req.getParameter("selRegion").toString();
		String calleDespacho			= req.getParameter("calle").toString();
		String numeroDespacho			= req.getParameter("numCalle").toString();
		if(null != req.getParameter("depto")){
			numeroDespacho				+= ", "+ req.getParameter("depto").toString();
		}
		clienteCO.setComunaDespacho(comunaDespacho);
		clienteCO.setRegionDespacho(regionDespacho);
		clienteCO.setCalleDespacho(calleDespacho);
		clienteCO.setNumeroDespacho(numeroDespacho);
		//Se sube a sesion
		getSession().put("Cliente", clienteCO);
		response = paso4Page;
		logMetricas.info("|PORTAL|ContratacionOnline|TIPO|INFO|USUARIO|"+clienteCO.getTelefono()+"|RUT|"+  clienteCO.getRut() + "|FLUJO|ContratacionOnline|ETAPA|Paso4|NUMEROSERIE|"+clienteCO.getnSerie()+"|NUMERONEGOCIO|"+ clienteCO.getNumeroNegocio() +"|CAPTCHA|true|NUMEROAPORTAR|"+clienteCO.getNumeroPorta() +"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|RESPUESTAEMAIL||CODIGOCAP||IMEI||");

		return response;
	}
	
	
	private Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}
	
}
