package com.esa.ponline.appmobile.web.actions;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.http.HttpServletRequest;


import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.helper.OfertaContratacionOnlineHelper;
import com.esa.ponline.appmobile.utils.FormatoMovilUtil;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.esa.ponline.appmobile.web.bean.ResValidarDatosClientesSIMMAUTVO;
import com.esa.ponline.appmobile.web.bean.ValidaDatosClientesVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;


import cl.esa.ponline.appmobile.delegate.DelegateServices;


public class ContratacionAction extends ActionSupport {
	 /**
	 * 
	 */
	private static final long serialVersionUID = 4716733355940579957L;
	private static final Logger LOGGER      = Logger.getLogger(ContratacionAction.class);
	private static final Logger	logMetricas	= Logger.getLogger("Metricas");
	private String paso1APage 			= "ContratacionPaso1A";
	private String paso1BPage 			= "ContratacionPaso1B";
	private String paso1ErrorPage 		= "ContratacionPaso1Error";
	private DelegateServices delegateServices = new DelegateServices();

	///////TEST
	public static final String secret = "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe";
	/////////////////
	//public static final String secret = "6LehBwQTAAAAAJ2FqVtT54UDQIwwLUh4OlLn_Azw";
	///////////////
	
	private Plan planCO;
	private Cliente clienteCO;
	
	private List<String> terminosYCondiciones;
	public String execute(){		
		
		planCO = new Plan();
		String response="";
		//Se obtienen los parametros que se entregaran en la URL desde el portal publico
		HttpServletRequest req = ServletActionContext.getRequest();
		String paso			= req.getParameter("paso").toString();
		//Se define si el paso a entregar sera A o B.
		if(paso.equals("1A")){
			response = contratacionPaso1A();
		}else if(paso.equals("1B")){
			response = contratacionPaso1B();
		}
		return response;
	}
	

	public String contratacionPaso1A() {
		LOGGER.info("Paso 1 A - Contratacion");
		String cargoDcto="";
		HttpServletRequest req = ServletActionContext.getRequest();
		
		cargoDcto = calculaDescuento(req.getParameter("CargoFijo"),"10");
		
		planCO.setNombrePlan(req.getParameter("Nombre").toString());
		planCO.setDatosPlan(req.getParameter("Datos").toString());
		planCO.setRrssPlan(req.getParameter("RRSS").toString());
		planCO.setMinutosPlan(req.getParameter("Minutos").toString());
		planCO.setCargoPlan(FormatoMovilUtil.formatoTextoDinero(Double.valueOf(req.getParameter("CargoFijo"))));
		planCO.setClickToCall(Config.getAppProperty("cliick_"+req.getParameter("CargoFijo")));
		planCO.setCargoDcto(FormatoMovilUtil.formatoTextoDinero(Double.valueOf(cargoDcto)));
		
		//Se deja en sesion el objeto PLAN
		getSession().put("Plan", planCO);
		
		//Obtenemos terminos y condiciones
		HashMap<String, String> hashMapTerminos = new HashMap<String, String>(Config.getConfigContratacionOnline_Terminos());
		if(null!=hashMapTerminos){
			setTerminosYCondiciones(
					OfertaContratacionOnlineHelper.cargarTerminosyDebesSaber(req.getParameter("CargoFijo"), 
							hashMapTerminos,"_COM_"));
		}else{
			LOGGER.info("Error al cargar terminos hashMapDebesLA>>> "+ hashMapTerminos);
		}
		getSession().put("terminosYCondiciones", terminosYCondiciones);

		//Definimos LINK de click to call
		logMetricas.info("|PORTAL|ContratacionOnline|TIPO|INFO|USUARIO||RUT||FLUJO|ContratacionOnline|ETAPA|Paso1A|NUMEROSERIE||NUMERONEGOCIO||CAPTCHA||");
		
		return paso1APage ;
	}
	
	public String contratacionPaso1B() {
		LOGGER.info("Paso 1 B - Contratacion");	
		
		boolean validaDatos = false;
        String nRut;
        String dRut;
        ResValidarDatosClientesSIMMAUTVO validarDatosCliente = new ResValidarDatosClientesSIMMAUTVO();
        ValidaDatosClientesVO validaDatosClientes = new ValidaDatosClientesVO();
		//Trae datos
		HttpServletRequest req = ServletActionContext.getRequest();
		String rut			= req.getParameter("rut");
		String nSerie		= req.getParameter("nSerie");
		String email		= req.getParameter("email");
		String telefono		= req.getParameter("telefono");
		
		///////////////////////////
		String recapchaResponse = req.getParameter("g-recaptcha-response");
		//boolean validaRecapcha = recapcha(recapchaResponse);
		boolean validaRecapcha = true;
		//System.out.println("validaRecapcha: "+validaRecapcha);
		//////////////////////////////

		if(validaRecapcha){
		
		
		nSerie = nSerie.replaceAll("\\.", "");
		rut = rut.replace(".", "");

	        String[] rutSplited = rut.replace(".", "").split("-");
	        if(rutSplited.length>1){
		        nRut			= rutSplited[0];
		        dRut         	= rutSplited[1];
		        
				validaDatosClientes.setCodPlataforma("ONL");
				validaDatosClientes.setMsisdn(FormatoMovilUtil.obtieneFormatoMovilPorLargo(telefono));
				validaDatosClientes.setNumeroMovilVirtual("");
				validaDatosClientes.setNumeroSerie(nSerie);
				validaDatosClientes.setTipoNegocio(2);
				validaDatosClientes.setDigitoVerificador(dRut);
				validaDatosClientes.setRutClienteSinDV(Integer.valueOf(nRut));
	        }else{
	        	logMetricas.error("|PORTAL|ContratacionOnline|TIPO|ERROR|USUARIO|"+telefono+"|RUT|"+  rut + "|FLUJO|ContratacionOnline|ETAPA|Paso1B|NUMEROSERIE|"+nSerie+"|NUMERONEGOCIO|"+ validarDatosCliente.getRespuestaValidarCliente().getNumeroNegocio() +"|CAPTCHA|"+validaRecapcha+"|");
	        	return paso1ErrorPage;
	        }
	        try {
	        	validarDatosCliente = delegateServices.validarDatosClientesSimAut(validaDatosClientes);
			} catch (Exception e) {
				LOGGER.error("Error Servicio Evaluacion Comercial | Rut : " + nRut+dRut + "|");
				logMetricas.error("|PORTAL|ContratacionOnline|TIPO|ERROR|USUARIO|"+telefono+"|RUT|"+  nRut+dRut + "|FLUJO|ContratacionOnline|ETAPA|Paso1B|NUMEROSERIE|"+nSerie+"|NUMERONEGOCIO|"+ validarDatosCliente.getRespuestaValidarCliente().getNumeroNegocio() +"|CAPTCHA|"+validaRecapcha+"|");
				e.printStackTrace();
			}
	        if(validarDatosCliente.getRespuestaValidarCliente().getIndicadorExito().equals("O-000")){
	        	validaDatos = true;
	        }
	        
			if(!validaDatos){
				logMetricas.error("|PORTAL|ContratacionOnline|TIPO|ERROR|USUARIO|"+telefono+"|RUT|"+  nRut+dRut + "|FLUJO|ContratacionOnline|ETAPA|Paso1B|NUMEROSERIE|"+nSerie+"|NUMERONEGOCIO|"+ validarDatosCliente.getRespuestaValidarCliente().getNumeroNegocio() +"|CAPTCHA|"+validaRecapcha+"|");
				return paso1ErrorPage;
			}else{
				//Se setean los parametros enviados del formulario 1A, paramostrar en 1B
				clienteCO = new Cliente();
				clienteCO.setRut(rut);
				clienteCO.setnSerie(nSerie);
				clienteCO.setEmail(email);
				clienteCO.setTelefono(telefono);
				clienteCO.setNombre(validarDatosCliente.getRespuestaValidarCliente().getNombresCliente());
				clienteCO.setApellidos(validarDatosCliente.getRespuestaValidarCliente().getApellidoPaterno());
				clienteCO.setNumeroNegocio(validarDatosCliente.getRespuestaValidarCliente().getNumeroNegocio());
				//Parametros en sesion con nombre Cliente
				getSession().put("Cliente", clienteCO);
			}
			}else{
				logMetricas.error("|PORTAL|ContratacionOnline|TIPO|ERROR|USUARIO|"+telefono+"|RUT|"+  rut + "|FLUJO|ContratacionOnline|ETAPA|Paso1B|NUMEROSERIE|"+nSerie+"|NUMERONEGOCIO||CAPTCHA|"+validaRecapcha+"|");
				return paso1ErrorPage;
			}
			
			
			
			logMetricas.info("|PORTAL|ContratacionOnline|TIPO|INFO|USUARIO|"+telefono+"|RUT|"+  nRut+dRut + "|FLUJO|ContratacionOnline|ETAPA|Paso1B|NUMEROSERIE|"+nSerie+"|NUMERONEGOCIO|"+ validarDatosCliente.getRespuestaValidarCliente().getNumeroNegocio() +"|CAPTCHA|"+validaRecapcha+"|");
			
			return paso1BPage ;
	}
	
	public boolean recapcha(String gRecaptchaResponse){
		boolean valida = false;
		try{
			if (gRecaptchaResponse != null || !"".equals(gRecaptchaResponse)) {

				String urlString = "https://www.google.com/recaptcha/api/siteverify?secret="+secret+"&response="+gRecaptchaResponse; 
				URL url = new URL(null, urlString, new sun.net.www.protocol.https.Handler());
				//URL url = new URL(urlString);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				String line, outputString = "";
				BufferedReader reader = new BufferedReader(
	                    new InputStreamReader(conn.getInputStream()));
	            while ((line = reader.readLine()) != null) {
	                outputString += line;
	            }

				//parsea respuesta Json para sucess
				JsonReader jsonReader = Json.createReader(new StringReader(outputString));
				JsonObject jsonObject = jsonReader.readObject();
				jsonReader.close();
				
				valida = jsonObject.getBoolean("success");
			}
		}catch(Exception e){
			LOGGER.error("Error recapcha: "+e);
		}
		return valida;
	}
	
	private Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}


	public List<String> getTerminosYCondiciones() {
		return terminosYCondiciones;
	}


	public void setTerminosYCondiciones(List<String> terminosYCondiciones) {
		this.terminosYCondiciones = terminosYCondiciones;
	}  
	
	public String calculaDescuento(String cargoFijo, String dcto){
		Double cargoDcto = null;
		cargoDcto = Double.valueOf(cargoFijo) - (Double.valueOf(cargoFijo) * Integer.parseInt(dcto)/100);
		int cargo = cargoDcto.intValue();
		return String.valueOf(cargo);
	}
	
}
