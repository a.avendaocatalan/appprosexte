package com.esa.ponline.appmobile.web.actions;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.epcs.clientes.evaluacion.types.EvaluacionComercialNecResponseType;
import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.utils.FormatoMovilUtil;
import com.esa.ponline.appmobile.utils.PruebaUtil;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.ConsultaFactibilidadPortVO;
import com.esa.ponline.appmobile.web.bean.Plan;
import com.esa.ponline.appmobile.web.bean.ReqValidarDatosPortabilidadVO;
import com.esa.ponline.appmobile.web.bean.ResValidarDatosPortabilidadVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cl.esa.ponline.appmobile.delegate.DelegateServices;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultafactportacionsync.RespuestaConsultaFactPortacionSync;

public class ContratacionPaso3Action extends ActionSupport {
	private static final long serialVersionUID = -2586889159761729857L;
	private static final Logger LOGGER = Logger.getLogger(ContratacionAction.class);
	private static final Logger	logMetricas	= Logger.getLogger("Metricas");
	private String paso3Page 			= "ContratacionPaso3";
	private String paso3ErrorPage 		= "ContratacionPaso3Error";
	
	//private Plan planCO;
	private Cliente clienteCO;
	private List<String> comunas;
	private List<String> regiones;
	
	private DelegateServices delegateServices = new DelegateServices();
	
	
	public String execute(){
		String response="";
		HttpServletRequest req = ServletActionContext.getRequest();
		clienteCO = (Cliente) getSession().get("Cliente");
		String codCAP			= req.getParameter("codCap").toString();
		String imei		= req.getParameter("imei").toString();
		
		response = contratacionPaso3(codCAP, imei, clienteCO);
		if(!response.equals(paso3ErrorPage)){
			logMetricas.info("|PORTAL|ContratacionOnline|TIPO|INFO|USUARIO|"+clienteCO.getTelefono()+"|RUT|"+  clienteCO.getRut() + "|FLUJO|ContratacionOnline|ETAPA|Paso3|NUMEROSERIE|"+clienteCO.getnSerie()+"|NUMERONEGOCIO|"+ clienteCO.getNumeroNegocio() +"|CAPTCHA|true|NUMEROAPORTAR|"+clienteCO.getNumeroPorta() +"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|RESPUESTAEMAIL||CODIGOCAP|"+codCAP+"|IMEI|"+imei+"|");
		}else{
			logMetricas.error("|PORTAL|ContratacionOnline|TIPO|ERROR|USUARIO|"+clienteCO.getTelefono()+"|RUT|"+  clienteCO.getRut() + "|FLUJO|ContratacionOnline|ETAPA|Paso3|NUMEROSERIE|"+clienteCO.getnSerie()+"|NUMERONEGOCIO|"+ clienteCO.getNumeroNegocio() +"|CAPTCHA|true|NUMEROAPORTAR|"+clienteCO.getNumeroPorta() +"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|RESPUESTAEMAIL||CODIGOCAP|"+codCAP+"|IMEI|"+imei+"|");
		}
		return response;
	}

	private String contratacionPaso3(String codCAP, String imei, Cliente clienteCO) {
		String response = paso3ErrorPage;
		String reg = Config.getAppProperty("regiones");
		List<String> regiones = Arrays.asList(reg.split(","));
		setRegiones(regiones);
		
		if(Boolean.valueOf(Config.getAppProperty("switchPRueba"))){
			PruebaUtil.creaDatosPrueba();
			response = paso3Page;
		}else{
			if(validaDatosPortabilidad(codCAP, imei, clienteCO)){
				if(factibilidadPortabilidad(codCAP, imei, clienteCO)){
					if(evaluacionComercial(clienteCO)){
						response = paso3Page;
					}
				}
			}
		}
		return response;
	} 
	
	private boolean validaDatosPortabilidad(String codCAP, String imei, Cliente clienteCO){
		boolean resp = false;
		ReqValidarDatosPortabilidadVO req = new ReqValidarDatosPortabilidadVO();
		ResValidarDatosPortabilidadVO res = new ResValidarDatosPortabilidadVO();
		
		LOGGER.info("-------------validarDatosPortabilidad---------------");
		LOGGER.info("codCAP: "+ codCAP);
		LOGGER.info("MercadoOrigen: "+ clienteCO.getMercadoOrigen());
		LOGGER.info("imei: "+ imei);
		LOGGER.info("NumeroPorta: "+FormatoMovilUtil.obtieneFormatoMovilPorLargo(clienteCO.getNumeroPorta()) );
		LOGGER.info("NumeroNegocio: "+clienteCO.getNumeroNegocio() );
		LOGGER.info("-------------validarDatosPortabilidad---------------");
		
		req.setCap(codCAP);
		req.setCodMercadoOrigen(String.valueOf(clienteCO.getMercadoOrigen()));
		req.setCodPlataforma("ONL");
		req.setImei(imei);
		req.setNumeroMovil(FormatoMovilUtil.obtieneFormatoMovilPorLargo(clienteCO.getNumeroPorta()));
		req.setNumeroNegocio(clienteCO.getNumeroNegocio());

		try {
			res = delegateServices.validarDatosPortabilidad(req);
		} catch (Exception e) {
			LOGGER.error("Error al llamar servicio Validar Datos Portabilidad");
			e.printStackTrace();
		}
		LOGGER.info("RESULTADO DE LA CONSULTA DEL SERVICIO: "+res.getIndicadorExito()); 
		LOGGER.info("descrición: "+res.getDescripcion()); 
		if(res.getIndicadorExito().equals("000")){
			resp = true;
		}
		return resp;
	}
	
	private boolean factibilidadPortabilidad(String codCAP, String imei, Cliente clienteCO){
		boolean resp = false;
		ConsultaFactibilidadPortVO req = new ConsultaFactibilidadPortVO();
		RespuestaConsultaFactPortacionSync res = new RespuestaConsultaFactPortacionSync();
		
		req.setAni(FormatoMovilUtil.obtieneFormatoMovilPorLargo(clienteCO.getNumeroPorta()));
		req.setIdd("220");
		req.setImei(imei);
		
		try {
			res = delegateServices.consultaFactibilidadPortabilidad(req);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(res.getErrorCode()==0){
			resp = true;
		}
		return resp;
	}
	
	private boolean evaluacionComercial(Cliente clienteCO){
		EvaluacionComercialNecResponseType res = new EvaluacionComercialNecResponseType();
		boolean resp = false;
		String[] rutSplit = clienteCO.getRut().split("-");
		String nRut=rutSplit[0];
		String dRut=rutSplit[1];
		
		try {
			res = delegateServices.obtenerEvaluacionComercial(nRut, dRut);
			
		} catch (Exception e) {
			LOGGER.error("Error al llamar al servicio de Evaluacion comercial");
			e.printStackTrace();
		}
		
		if(res.getCodigoSalida().equals("0000")){
			//Lineas disponibles > 0
			if(Integer.parseInt(res.getNmroLineasDisponibles())>0){
				resp = true;
			}else{
				LOGGER.error("NO tiene lineas disponibles.");
			}
		}
		return resp;
	}
	
	private Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}

	public List<String> getComunas() {
		return comunas;
	}

	public void setComunas(List<String> comunas) {
		this.comunas = comunas;
	}
	
	public List<String> getRegiones(){
		return regiones;
	}
	
	public void setRegiones(List<String> regiones){
		this.regiones = regiones;
	}
		
}
