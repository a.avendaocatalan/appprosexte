package com.esa.ponline.appmobile.web.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.esa.crm.portabilidad.t.obtenercompaniamovil.Operator;
import com.esa.ponline.appmobile.config.Config;
import com.esa.ponline.appmobile.utils.FormatoMovilUtil;
import com.esa.ponline.appmobile.utils.PruebaUtil;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.DataEmailVO;
import com.esa.ponline.appmobile.web.bean.ReqConsultaTitularDeudaSyncVO;
import com.esa.ponline.appmobile.web.bean.ReqGeneraCapSuscripcionVO;
import com.esa.ponline.appmobile.web.bean.ResGeneraCapSuscripcionVO;
import com.esa.ponline.appmobile.web.bean.RespuestaGenerarCapVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cl.esa.ponline.appmobile.delegate.DelegateServices;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultatitulardeudasync.RespuestaConsultaTitularDeudaSync;

public class ContratacionPaso2Action extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4420653111614019364L;
	private static final Logger LOGGER = Logger.getLogger(ContratacionPaso2Action.class);
	private static final Logger	logMetricas	= Logger.getLogger("Metricas");
	private String paso2APage 			= "ContratacionPaso2A";
	private String paso2BPage 			= "ContratacionPaso2B";
	private String paso2ErrorPage 		= "ContratacionPaso2Error";
	private File[] cedulaFront;
	private Cliente clienteCO;
	
	private DelegateServices delegateServices = new DelegateServices();
	
	ReqConsultaTitularDeudaSyncVO titularidadDeudaVO;
	RespuestaConsultaTitularDeudaSync responseTitularidad;
	public String execute(){
		String response="";
		String numeroPorta = "";
		String compania = "";
		clienteCO = (Cliente) getSession().get("Cliente");
		//Se obtienen parametro de pasos con el fin de definir si va a pantalla A o B
		HttpServletRequest req = ServletActionContext.getRequest();
		String paso			= req.getParameter("paso").toString();		
		if(paso.equals("2A")){
			String nombres			= req.getParameter("nombreCliente").toString();
			String apellidos		= req.getParameter("apellidosCliente").toString();
			numeroPorta		        = req.getParameter("numeroPortar").toString();
			compania			    = req.getParameter("companiaMovil");
			String radio			= req.getParameter("radio-group");
			
			response = contratacionPaso2A(nombres, apellidos, numeroPorta, compania, radio);
			
		}else if(paso.equals("2B")){
			response = contratacionPaso2B();
		}		
		return response;
	}
	
	public String contratacionPaso2A(String nombres, String apellidos, String numeroPorta, String compania, String radio) {
		int modalidad = 0;
		String response = paso2ErrorPage;
		clienteCO = (Cliente) getSession().get("Cliente");
		
		clienteCO.setNombre(nombres);
		clienteCO.setApellidos(apellidos);
		clienteCO.setNumeroPorta(numeroPorta);
		
		if(radio.toUpperCase().equals("PREPAGO")){
			modalidad = 0;
		}else if(radio.toUpperCase().equals("PLAN")){
			modalidad = 1;
		}
		
		//Titularidad y deuda
		titularidadDeudaVO = new ReqConsultaTitularDeudaSyncVO();
		titularidadDeudaVO.setIdd("220");
		titularidadDeudaVO.setModalidad(modalidad);
		titularidadDeudaVO.setMsisdn(FormatoMovilUtil.obtieneFormatoMovilPorLargo(numeroPorta));
		titularidadDeudaVO.setRutSolicitante(clienteCO.getRut());
		titularidadDeudaVO.setRutTitular(clienteCO.getRut());
		titularidadDeudaVO.setTipoServicio(0);
		titularidadDeudaVO.setTipoServicioTo(0);
		
		responseTitularidad = new RespuestaConsultaTitularDeudaSync();
		try {
			responseTitularidad = delegateServices.consultaTitularDeudaSyncService(titularidadDeudaVO);
		} catch (Exception e) {
			e.printStackTrace();
			return paso2ErrorPage;
		}
		if(responseTitularidad.getRespuestasANI().get(0).getEstado() == 0){
			if(responseTitularidad.getRespuestasANI().get(0).getTitularidad() == 0){
				if(responseTitularidad.getRespuestasANI().get(0).getSaldoPendienteANI()<=0){
					//Obtener Compania movil
					compania = ObtenerCompaniaMovil(numeroPorta);
					if(null != compania  && !compania.contains("ENTEL")){
						clienteCO.setCompaniaOrigen(compania);
						//getSession().put("Plan", planCO);
						if(radio.toUpperCase().equals("PREPAGO")){
							if(generaCAPPrepago(numeroPorta))
								clienteCO.setMercadoOrigen(0);
								getSession().put("Cliente", clienteCO);
								response = paso2APage;
						}else if(radio.toUpperCase().equals("PLAN")){
							if(generaCAPPlan(numeroPorta))
								clienteCO.setMercadoOrigen(1);
								getSession().put("Cliente", clienteCO);
								response = paso2APage;
						}
					}else{
						LOGGER.error("Numero no existente o no tiene compania");
					}
				}else{
					LOGGER.error("El saldo pendiente no debe ser mayor a cero");
				}
			}else{
				LOGGER.error("El rut solicitante no es TITULAR");
			}
		}else{
			LOGGER.error("El estado es INACTIVO");
		}
		if(!response.equals(paso2ErrorPage)){
			logMetricas.info("|PORTAL|ContratacionOnline|TIPO|INFO|USUARIO|"+clienteCO.getTelefono()+"|RUT|"+  clienteCO.getRut() + "|FLUJO|ContratacionOnline|ETAPA|Paso2A|NUMEROSERIE|"+clienteCO.getnSerie()+"|NUMERONEGOCIO|"+ clienteCO.getNumeroNegocio() +"|CAPTCHA|true|NUMEROAPORTAR|"+clienteCO.getNumeroPorta() +"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|RESPUESTAEMAIL||");
		}else{
			logMetricas.error("|PORTAL|ContratacionOnline|TIPO|ERROR|USUARIO|"+clienteCO.getTelefono()+"|RUT|"+  clienteCO.getRut() + "|FLUJO|ContratacionOnline|ETAPA|Paso2A|NUMEROSERIE|"+clienteCO.getnSerie()+"|NUMERONEGOCIO|"+ clienteCO.getNumeroNegocio() +"|CAPTCHA|true|NUMEROAPORTAR|"+clienteCO.getNumeroPorta() +"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|RESPUESTAEMAIL||");
		}	
	return response;
	}
	
	public String contratacionPaso2B() {			
		File cedulaDelantera = null;
		File cedulaTrasera = null;
		int cont = 1;
		String respuesta = "";
		LOGGER.info("\n\n Imagenes Cedula de Identidad");
		LOGGER.info("Archivos:");
		for (File u : cedulaFront) {
			LOGGER.info("Archivo : " + u.getName() + "\t" +"Tama�o archivo :"+ u.length());					
			if(cont == 1 ){cedulaDelantera = u;}
			if(cont == 2 ){cedulaTrasera = u;}
			cont++;
		}
		try{
			respuesta = guardarImagenesEmail(cedulaDelantera,cedulaTrasera);
		}catch (Exception e) {
			LOGGER.error("Al guardar las imagenes via correo electronico");
		}
		if(Boolean.valueOf(Config.getAppProperty("switchPRueba"))){
			PruebaUtil.creaDatosPrueba();
		}
		logMetricas.info("|PORTAL|ContratacionOnline|TIPO|INFO|USUARIO|"+clienteCO.getTelefono()+"|RUT|"+  clienteCO.getRut() + "|FLUJO|ContratacionOnline|ETAPA|Paso2B|NUMEROSERIE|"+clienteCO.getnSerie()+"|NUMERONEGOCIO|"+ clienteCO.getNumeroNegocio() +"|CAPTCHA|true|NUMEROAPORTAR|"+clienteCO.getNumeroPorta() +"|COMPANIA|"+clienteCO.getCompaniaOrigen()+"|RESPUESTAEMAIL|"+respuesta+"|");
		return paso2BPage;
	}
	
	private byte[] cargaArchivo(File archivo) throws IOException {
		InputStream is = new FileInputStream(archivo);

	    long length = archivo.length();
	    if (length > Integer.MAX_VALUE) {
	        // File is too large
	    }
	    byte[] bytes = new byte[(int)length];

	    int offset = 0;
	    int numRead = 0;
	    while (offset < bytes.length
	           && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
	        offset += numRead;
	    }

	    if (offset < bytes.length) {
	        throw new IOException("No se puede leer el archivo :  "+archivo.getName());
	    }

	    is.close();
	    return bytes;
	}
	private String convertirImagenAString(File imagen) throws IOException {
		String imagenTexto = "";
		byte[] bytes = null;
		bytes = cargaArchivo(imagen);		
		byte[] encoded = Base64.encodeBase64(bytes);
		String encodedString = new String(encoded);
		imagenTexto = encodedString;
		return imagenTexto;
	}
	private String guardarImagenesEmail(File cedulaDelantera, File cedulaTrasera) {
		String to = "SER_flujomovil1@entel.cl";
		String from= "cedulascontratacion@entel.cl";
		String subject= "Cedulas contratacion";
		String body= "Estimado, recuerde cambiar la extension del archivo de .bin a .jpg para poder visualizar las imagenes. Saludos";
		String tImagen = "IMAGE";
		List<DataEmailVO> adjunt = new ArrayList<DataEmailVO>();
		String respuestaEmail = "";
		String cedulaDelanteraS = "";
		String cedulaTraseraS = "";
		try {
			cedulaDelanteraS = convertirImagenAString(cedulaDelantera);
			cedulaTraseraS   = convertirImagenAString(cedulaTrasera);
		} catch (IOException e1) {
			LOGGER.error("Error al convertir la imagen a texto");
			e1.printStackTrace();
		}
		
		DataEmailVO cedulaD = new DataEmailVO();
		cedulaD.setData(cedulaDelanteraS);
		cedulaD.setNombre("CedulaDelantera");
		cedulaD.setTipo(tImagen);
		adjunt.add(cedulaD);
		DataEmailVO cedulaT = new DataEmailVO();		
		cedulaT.setData(cedulaTraseraS);
		cedulaT.setNombre("CedulaTrasera");
		cedulaT.setTipo(tImagen);
		adjunt.add(cedulaT);
		//Envio Email con adjuntos
		try {
			respuestaEmail = delegateServices.enviaEmail(to, from, subject, body, adjunt);
		} catch (Exception e) {
			LOGGER.error("Error al enviar las imagenes de la cedula por email");
			e.printStackTrace();
		}
		return respuestaEmail;
	}
	private Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}
	
	private String ObtenerCompaniaMovil(String movilCliente){
		String compania = null;
		movilCliente = FormatoMovilUtil.obtieneFormatoMovilPorLargo(movilCliente);
		try {
			Operator op = new Operator();
			op = delegateServices.obtieneCompania(movilCliente);
			if(!op.getOperatorDsc().equals("UNK")){
				compania = op.getOperatorDsc();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return compania;
	}
	
	private Boolean generaCAPPrepago(String movilCliente){
		RespuestaGenerarCapVO respGeneraCAP = new RespuestaGenerarCapVO();
		movilCliente = FormatoMovilUtil.obtieneFormatoMovilPorLargo(movilCliente);
		Boolean resp = false;
		try {
			respGeneraCAP = delegateServices.generarCap(movilCliente, "220");
			if(respGeneraCAP.getErrorCode().equals("0")){
				resp=true;
			}
		} catch (Exception e) {
			resp = false;
			e.printStackTrace();
		}
		return resp;
	}
	
	private Boolean generaCAPPlan(String movilCliente){
		ReqGeneraCapSuscripcionVO req = new ReqGeneraCapSuscripcionVO();
		ResGeneraCapSuscripcionVO respGeneraCAP = new ResGeneraCapSuscripcionVO();
		movilCliente = FormatoMovilUtil.obtieneFormatoMovilPorLargo(movilCliente);
		//Setea datos del request
		req.setNumeroMovil(movilCliente);
		req.setCodPlataforma("ONL");
		Boolean resp = false;
		try {
			respGeneraCAP = delegateServices.generarCapSuscripcion(req);
			if(respGeneraCAP.getIndicadorExito().equals("O-000")){
				resp = true;
			}
		} catch (Exception e) {
			resp = false;
			e.printStackTrace();
		}
		return resp;
	}

	
	
	public File[] getCedulaFront() {
		return cedulaFront;
	}

	public void setCedulaFront(File[] cedulaFront) {
		this.cedulaFront = cedulaFront;
	}

}
