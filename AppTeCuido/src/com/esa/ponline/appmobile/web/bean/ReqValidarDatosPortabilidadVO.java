package com.esa.ponline.appmobile.web.bean;

import java.io.Serializable;

public class ReqValidarDatosPortabilidadVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1480142369722244559L;
	
	private String codPlataforma;
    private String cap;
    private String numeroNegocio;
    private String numeroMovil;
    private String imei;
    private String codMercadoOrigen;
    
	public String getCodPlataforma() {
		return codPlataforma;
	}
	public void setCodPlataforma(String codPlataforma) {
		this.codPlataforma = codPlataforma;
	}
	public String getCap() {
		return cap;
	}
	public void setCap(String cap) {
		this.cap = cap;
	}
	public String getNumeroNegocio() {
		return numeroNegocio;
	}
	public void setNumeroNegocio(String numeroNegocio) {
		this.numeroNegocio = numeroNegocio;
	}
	public String getNumeroMovil() {
		return numeroMovil;
	}
	public void setNumeroMovil(String numeroMovil) {
		this.numeroMovil = numeroMovil;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getCodMercadoOrigen() {
		return codMercadoOrigen;
	}
	public void setCodMercadoOrigen(String codMercadoOrigen) {
		this.codMercadoOrigen = codMercadoOrigen;
	}
    
    
}
