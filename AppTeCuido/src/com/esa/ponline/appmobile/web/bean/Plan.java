package com.esa.ponline.appmobile.web.bean;

public class Plan {
	private String idPlan;
	private String nombrePlan;
	private String datosPlan;
	private String rrssPlan;
	private String minutosPlan;
	private String cargoPlan;
	private String cargoDcto;
	private String clickToCall;
	
	public String getIdPlan() {
		return idPlan;
	}
	public void setIdPlan(String idPlan) {
		this.idPlan = idPlan;
	}
	public String getNombrePlan() {
		return nombrePlan;
	}
	public void setNombrePlan(String nombrePlan) {
		this.nombrePlan = nombrePlan;
	}
	public String getDatosPlan() {
		return datosPlan;
	}
	public void setDatosPlan(String datosPlan) {
		this.datosPlan = datosPlan;
	}
	public String getRrssPlan() {
		return rrssPlan;
	}
	public void setRrssPlan(String rrssPlan) {
		this.rrssPlan = rrssPlan;
	}
	public String getMinutosPlan() {
		return minutosPlan;
	}
	public void setMinutosPlan(String minutosPlan) {
		this.minutosPlan = minutosPlan;
	}
	public String getCargoPlan() {
		return cargoPlan;
	}
	public void setCargoPlan(String cargoPlan) {
		this.cargoPlan = cargoPlan;
	}
	public String getClickToCall() {
		return clickToCall;
	}
	public void setClickToCall(String clickToCall) {
		this.clickToCall = clickToCall;
	}
	public String getCargoDcto() {
		return cargoDcto;
	}
	public void setCargoDcto(String cargoDcto) {
		this.cargoDcto = cargoDcto;
	}

}
