package com.esa.ponline.appmobile.web.bean;

import java.io.Serializable;

public class ValidaDatosClientesVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 781886590248467507L;
	private String codPlataforma;
	private int tipoNegocio;
	private int rutClienteSinDV;
	private String digitoVerificador;
	private String numeroSerie;
	private String numeroMovilVirtual;
	private String msisdn;
	public String getCodPlataforma() {
		return codPlataforma;
	}
	public void setCodPlataforma(String codPlataforma) {
		this.codPlataforma = codPlataforma;
	}
	public int getTipoNegocio() {
		return tipoNegocio;
	}
	public void setTipoNegocio(int tipoNegocio) {
		this.tipoNegocio = tipoNegocio;
	}
	public int getRutClienteSinDV() {
		return rutClienteSinDV;
	}
	public void setRutClienteSinDV(int rutClienteSinDV) {
		this.rutClienteSinDV = rutClienteSinDV;
	}
	public String getDigitoVerificador() {
		return digitoVerificador;
	}
	public void setDigitoVerificador(String digitoVerificador) {
		this.digitoVerificador = digitoVerificador;
	}
	public String getNumeroSerie() {
		return numeroSerie;
	}
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	public String getNumeroMovilVirtual() {
		return numeroMovilVirtual;
	}
	public void setNumeroMovilVirtual(String numeroMovilVirtual) {
		this.numeroMovilVirtual = numeroMovilVirtual;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	
}
