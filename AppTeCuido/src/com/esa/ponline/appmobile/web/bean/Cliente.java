package com.esa.ponline.appmobile.web.bean;

import java.io.Serializable;

public class Cliente implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3754731845683711593L;
	private String nombre;
	private String apellidos;
	private String rut;
	private String nSerie;
	private String telefono;
	private String email;
	private String companiaOrigen;
	private String numeroNegocio;
	private String numeroPorta;
	private Integer mercadoOrigen;
	private String comunaDespacho;
	private String regionDespacho;
	private String calleDespacho;
	private String numeroDespacho;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getnSerie() {
		return nSerie;
	}
	public void setnSerie(String nSerie) {
		this.nSerie = nSerie;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCompaniaOrigen() {
		return companiaOrigen;
	}
	public void setCompaniaOrigen(String companiaOrigen) {
		this.companiaOrigen = companiaOrigen;
	}
	public String getNumeroNegocio(){
		return this.numeroNegocio;
	}
	public void setNumeroNegocio(String numeroNegocio){
		this.numeroNegocio = numeroNegocio;
	}
	public String getNumeroPorta() {
		return numeroPorta;
	}
	public void setNumeroPorta(String numeroPorta) {
		this.numeroPorta = numeroPorta;
	}
	public Integer getMercadoOrigen() {
		return mercadoOrigen;
	}
	public void setMercadoOrigen(Integer mercadoOrigen) {
		this.mercadoOrigen = mercadoOrigen;
	}
	public String getComunaDespacho() {
		return comunaDespacho;
	}
	public void setComunaDespacho(String comunaDespacho) {
		this.comunaDespacho = comunaDespacho;
	}
	public String getRegionDespacho() {
		return regionDespacho;
	}
	public void setRegionDespacho(String regionDespacho) {
		this.regionDespacho = regionDespacho;
	}
	public String getCalleDespacho() {
		return calleDespacho;
	}
	public void setCalleDespacho(String calleDespacho) {
		this.calleDespacho = calleDespacho;
	}
	public String getNumeroDespacho() {
		return numeroDespacho;
	}
	public void setNumeroDespacho(String numeroDespacho) {
		this.numeroDespacho = numeroDespacho;
	}
	
}
