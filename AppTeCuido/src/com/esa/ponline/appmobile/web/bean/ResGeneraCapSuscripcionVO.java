package com.esa.ponline.appmobile.web.bean;

import java.io.Serializable;

public class ResGeneraCapSuscripcionVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 704583541948531963L;
	private String descripcion;
	private String indicadorExito;
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getIndicadorExito() {
		return indicadorExito;
	}
	public void setIndicadorExito(String indicadorExito) {
		this.indicadorExito = indicadorExito;
	}
	
	
	
}
