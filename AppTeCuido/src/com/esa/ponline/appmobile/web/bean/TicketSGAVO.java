package com.esa.ponline.appmobile.web.bean;

import java.io.Serializable;

public class TicketSGAVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -226030471864853950L;
	private String celularReclamado;
	private String motivo;
	private String mundo;
	private String nroSolicitud;
	private String plataforma;
	private String requerimientoCliente;
	private String solicitud;
	private String suboperacion;
	private String tipo;
	private String valor;
	private String workflow;
	public String getCelularReclamado() {
		return celularReclamado;
	}
	public void setCelularReclamado(String celularReclamado) {
		this.celularReclamado = celularReclamado;
	}
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public String getMundo() {
		return mundo;
	}
	public void setMundo(String mundo) {
		this.mundo = mundo;
	}
	public String getNroSolicitud() {
		return nroSolicitud;
	}
	public void setNroSolicitud(String nroSolicitud) {
		this.nroSolicitud = nroSolicitud;
	}
	public String getPlataforma() {
		return plataforma;
	}
	public void setPlataforma(String plataforma) {
		this.plataforma = plataforma;
	}
	public String getRequerimientoCliente() {
		return requerimientoCliente;
	}
	public void setRequerimientoCliente(String requerimientoCliente) {
		this.requerimientoCliente = requerimientoCliente;
	}
	public String getSolicitud() {
		return solicitud;
	}
	public void setSolicitud(String solicitud) {
		this.solicitud = solicitud;
	}
	public String getSuboperacion() {
		return suboperacion;
	}
	public void setSuboperacion(String suboperacion) {
		this.suboperacion = suboperacion;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public String getWorkflow() {
		return workflow;
	}
	public void setWorkflow(String workflow) {
		this.workflow = workflow;
	}
	
	
}
