package com.esa.ponline.appmobile.web.bean;

import java.io.Serializable;

import com.esa.marketsales.salesmassive.n.validardatosclientesimaut.fault.ValidarDatosClienteSIMAUTFault;
import com.esa.marketsales.salesmassive.n.validardatosclientesimaut.response.ValidarDatosClienteSIMAUTResponse;

public class ResValidarDatosClientesSIMMAUTVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4984067582109186496L;
	private ValidarDatosClienteSIMAUTResponse respuestaValidarCliente;
	private ValidarDatosClienteSIMAUTFault errorValidarCliente;
	
	public ValidarDatosClienteSIMAUTResponse getRespuestaValidarCliente() {
		return respuestaValidarCliente;
	}
	public void setRespuestaValidarCliente(ValidarDatosClienteSIMAUTResponse respuestaValidarCliente) {
		this.respuestaValidarCliente = respuestaValidarCliente;
	}
	public ValidarDatosClienteSIMAUTFault getErrorValidarCliente() {
		return errorValidarCliente;
	}
	public void setErrorValidarCliente(ValidarDatosClienteSIMAUTFault errorValidarCliente) {
		this.errorValidarCliente = errorValidarCliente;
	}

}
