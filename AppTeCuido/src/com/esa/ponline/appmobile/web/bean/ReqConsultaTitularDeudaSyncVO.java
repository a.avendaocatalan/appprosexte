package com.esa.ponline.appmobile.web.bean;

import java.io.Serializable;

public class ReqConsultaTitularDeudaSyncVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2628653765784248910L;
	private String idd;
	private int modalidad;
	private String rutSolicitante;
	private String RutTitular;
	private int tipoServicio;
	private int tipoServicioTo;
	private String msisdn;
	public String getIdd() {
		return idd;
	}
	public void setIdd(String idd) {
		this.idd = idd;
	}
	public int getModalidad() {
		return modalidad;
	}
	public void setModalidad(int modalidad) {
		this.modalidad = modalidad;
	}
	public String getRutSolicitante() {
		return rutSolicitante;
	}
	public void setRutSolicitante(String rutSolicitante) {
		this.rutSolicitante = rutSolicitante;
	}
	public String getRutTitular() {
		return RutTitular;
	}
	public void setRutTitular(String rutTitular) {
		RutTitular = rutTitular;
	}
	public int getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(int tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	public int getTipoServicioTo() {
		return tipoServicioTo;
	}
	public void setTipoServicioTo(int tipoServicioTo) {
		this.tipoServicioTo = tipoServicioTo;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	
	
}
