package com.esa.ponline.appmobile.web.bean;

import java.io.Serializable;

public class ResTicketSGAVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2680421495457451060L;
	private String codigo;
	private String mensaje;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	
}
