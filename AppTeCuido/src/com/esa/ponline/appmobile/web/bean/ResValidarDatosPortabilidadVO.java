package com.esa.ponline.appmobile.web.bean;

import java.io.Serializable;

public class ResValidarDatosPortabilidadVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1136750324130959096L;
	private String descripcion;
	private String indicadorExito;
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getIndicadorExito() {
		return indicadorExito;
	}
	public void setIndicadorExito(String indicadorExito) {
		this.indicadorExito = indicadorExito;
	}
	
	
}
