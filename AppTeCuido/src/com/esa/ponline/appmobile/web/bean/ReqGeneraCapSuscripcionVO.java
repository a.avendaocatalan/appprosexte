package com.esa.ponline.appmobile.web.bean;

import java.io.Serializable;

public class ReqGeneraCapSuscripcionVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4949251480215709857L;
	private String codPlataforma;
    private String numeroMovil;
    
	public String getCodPlataforma() {
		return codPlataforma;
	}
	public void setCodPlataforma(String codPlataforma) {
		this.codPlataforma = codPlataforma;
	}
	public String getNumeroMovil() {
		return numeroMovil;
	}
	public void setNumeroMovil(String numeroMovil) {
		this.numeroMovil = numeroMovil;
	}
    
}
