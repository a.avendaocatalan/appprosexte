package com.esa.ponline.appmobile.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.epcs.clientes.evaluacion.EvaluacionComercialNecFaultMessage;
import com.epcs.clientes.evaluacion.EvaluacionComercialNecPortType;
import com.epcs.clientes.evaluacion.EvaluacionComercialNecService;
import com.epcs.clientes.evaluacion.types.EvaluacionComercialNecRequestType;
import com.epcs.clientes.evaluacion.types.EvaluacionComercialNecResponseType;
import com.epcs.integracionit.flujotrabajo.EnviarMailService;
import com.epcs.integracionit.flujotrabajo.beans.ListMailBeanLiteral;
import com.epcs.integracionit.flujotrabajo.beans.MailBean;
import com.esa.cliente.contacto.sga.TicketSGAService;
import com.esa.cliente.contacto.sga.types.CreaTicketSgaRequestType;
import com.esa.cliente.contacto.sga.types.CrearTicketSgaResponseType;
import com.esa.crm.portabilidad.t.obtenercompaniamovil.Operator;
import com.esa.crm.portabilidad.t.obtenercompaniamovil.ServiceException_Exception;
import com.esa.crm.portabilidad.t.obtenercompaniamovil.ValidaMsisdn;
import com.esa.crm.portabilidad.t.obtenercompaniamovil.ValidaMsisdnService;
import com.esa.marketsales.salesmassive.n.enviarcapinternosuscripcion.EnviarCapInternoSuscripcion;
import com.esa.marketsales.salesmassive.n.enviarcapinternosuscripcion.request.EnviarCapInternoSuscripcionRequest;
import com.esa.marketsales.salesmassive.n.enviarcapinternosuscripcion.response.EnviarCapInternoSuscripcionResponse;
import com.esa.marketsales.salesmassive.n.validardatosclientesimaut.ValidarDatosClienteSIMAUT;
import com.esa.marketsales.salesmassive.n.validardatosclientesimaut.ValidarDatosClienteSIMAUTFaultMessage;
import com.esa.marketsales.salesmassive.n.validardatosclientesimaut.request.ValidarDatosClienteSIMAUTRequest;
import com.esa.marketsales.salesmassive.n.validardatosclientesimaut.response.ValidarDatosClienteSIMAUTResponse;
import com.esa.marketsales.salesmassive.n.validardatosportabilidadsimaut.ValidarDatosPortabilidadSIMAUT;
import com.esa.marketsales.salesmassive.n.validardatosportabilidadsimaut.request.ValidarDatosPortabilidadSIMAUTRequest;
import com.esa.marketsales.salesmassive.n.validardatosportabilidadsimaut.response.ValidarDatosPortabilidadSIMAUTResponse;
import com.esa.ponline.appmobile.interfaces.IServicesInterfaces;
import com.esa.ponline.appmobile.utils.ServiceUtils;
import com.esa.ponline.appmobile.utils.TimeWatch;
import com.esa.ponline.appmobile.web.bean.Cliente;
import com.esa.ponline.appmobile.web.bean.ConsultaFactibilidadPortVO;
import com.esa.ponline.appmobile.web.bean.DataEmailVO;
import com.esa.ponline.appmobile.web.bean.ReqConsultaTitularDeudaSyncVO;
import com.esa.ponline.appmobile.web.bean.ReqGeneraCapSuscripcionVO;
import com.esa.ponline.appmobile.web.bean.ReqValidarDatosPortabilidadVO;
import com.esa.ponline.appmobile.web.bean.ResGeneraCapSuscripcionVO;
import com.esa.ponline.appmobile.web.bean.ResTicketSGAVO;
import com.esa.ponline.appmobile.web.bean.ResValidarDatosClientesSIMMAUTVO;
import com.esa.ponline.appmobile.web.bean.ResValidarDatosPortabilidadVO;
import com.esa.ponline.appmobile.web.bean.RespuestaGenerarCapVO;
import com.esa.ponline.appmobile.web.bean.TicketSGAVO;
import com.esa.ponline.appmobile.web.bean.ValidaDatosClientesVO;
import com.esa.ponline.appmobile.ws.EntelServices;
import com.esa.ponline.appmobile.ws.EntelServicesLocatorException;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.finder.ClassFinder.Info;

import cl.iecisa.gateway.ws.integrator.services.ssgp_consultafactportacionsync.NumeroTelefonoDetalle;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultafactportacionsync.ParamConsultaFactPortacionSync;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultafactportacionsync.RespuestaConsultaFactPortacionSync;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultafactportacionsync.SSGPConsultaFactPortacionSyncException_Exception;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultafactportacionsync.SSGPConsultaFactPortacionSyncService;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultatitulardeudasync.NumeroTelefono;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultatitulardeudasync.ParamConsultaTitularDeudaSync;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultatitulardeudasync.RespuestaConsultaTitularDeudaSync;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultatitulardeudasync.SSGPConsultaTitularDeudaSyncException_Exception;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultatitulardeudasync.SSGPConsultaTitularDeudaSyncService;
import cl.iecisa.gateway.ws.integrator.services.ssgp_solicitacapasync.ParamSolicitaCAPAsync;
import cl.iecisa.gateway.ws.integrator.services.ssgp_solicitacapasync.RespuestaSolicitaCAPAsync;
import cl.iecisa.gateway.ws.integrator.services.ssgp_solicitacapasync.SSGPSolicitaCAPAsyncService;

public class ServicesImplements implements IServicesInterfaces {
	private static final Logger LOGGER 	= Logger.getLogger(ServicesImplements.class);
	private static final Logger	logPerformance	= Logger.getLogger("Performance");
	//private static final Logger log 	= Logger.getLogger("login");
	@Override
	public EvaluacionComercialNecResponseType obtenerEvaluacionComercial(String nRut, String dRut) {
		LOGGER.info("obtenerEvaluacionComercial");
		String codiTipoEvaluacion = "0";
		String plataforma = "ONL";
		EvaluacionComercialNecPortType port = null;
		EvaluacionComercialNecRequestType request;
		EvaluacionComercialNecResponseType response = null;
		try {
			LOGGER.info("Instanciamos locator --> Execute");
			EvaluacionComercialNecService locator = EntelServices.getEvaluacionComercial();
			LOGGER.info("Instanciamos el port --> Execute");
			TimeWatch watch = TimeWatch.start();
			port = locator.getEvaluacionComercialNecServicePort();
			LOGGER.info("Llenamos el request --> Execute");
			request = new EvaluacionComercialNecRequestType();
			request.setNrutCliente(nRut);
			request.setDrutCliente(dRut);
			request.setCodiTipoEvaluacion(codiTipoEvaluacion);
			request.setPlataforma(plataforma);
			request.setCodiGrupoCliente("");
			request.setCodiInstitucion("");
			request.setCodiInstitucionSIBF("");
			request.setCodiPuntoVenta("");
			request.setCodiSegmentoCliente("");
			request.setCodiSerieRutCliente("");
			request.setCodiTipoCliente("");
			request.setDescIPv4("");
			request.setDescNombreCliente("");
			request.setFlagCheque("");
			request.setMntoAcreditacion("");
			request.setNmroCheque("");
			request.setNmroCtaCte("");
			
			LOGGER.info("Se consulta el servicio --> Execute");
			response = new EvaluacionComercialNecResponseType();
			response = port.evaluacionComercial(request);
			LOGGER.info("Respuesta Evaluacion Comercial Cliente TIEMPO: "+ watch.time());
			LOGGER.info("Respuesta Evaluacion Comercial Codigo Salida: " + response.getCodigoSalida());
			LOGGER.info("Respuesta Evaluacion Comercial Lineas disponibles: " + response.getNmroLineasDisponibles());	
		
		} catch (EntelServicesLocatorException e) {
			LOGGER.error("Error al consultar servicio Evaluacion Comercial");
			e.printStackTrace();
		} catch (EvaluacionComercialNecFaultMessage e) {
			LOGGER.error("Error al consultar servicio Evaluacion Comercial");
			e.printStackTrace();
		}
		
		return response;
	}
	@Override
	public ResValidarDatosClientesSIMMAUTVO validarDatosClientesSimAut(ValidaDatosClientesVO validaDatosClientes){
		ResValidarDatosClientesSIMMAUTVO res = new ResValidarDatosClientesSIMMAUTVO();
		ValidarDatosClienteSIMAUTRequest req = new ValidarDatosClienteSIMAUTRequest();
		req.setTipoNegocio(validaDatosClientes.getTipoNegocio());
		req.setNumeroMovilVirtual(validaDatosClientes.getNumeroMovilVirtual());
		req.setNRutCliente(validaDatosClientes.getRutClienteSinDV());
		req.setDRutCliente(validaDatosClientes.getDigitoVerificador());
		req.setCodPlataforma(validaDatosClientes.getCodPlataforma());
		req.setCodiSerieRutCliente(validaDatosClientes.getNumeroSerie());
		TimeWatch watch = TimeWatch.start();
		try{
			LOGGER.info("Inicio Valdiacion Datos Cliente");
			ValidarDatosClienteSIMAUT port = EntelServices.getValidaDatosClienteSIMAUT();
			res.setRespuestaValidarCliente( port.getValidarDatosClienteSIMAUTPort().validarDatosClienteSIMAUT(req) );
			logPerformance.info("[MOVIL "+validaDatosClientes.getMsisdn()+"]|[ValidarDatosClientesSimAut]|[CODIGO "+res.getRespuestaValidarCliente().getIndicadorExito()+"]|[TIEMPO "+watch.time()+"]");
			LOGGER.info("Respuesta Valdiacion Datos Cliente TIEMPO: "+ watch.time());
			LOGGER.info("Respuesta Valdiacion Datos Cliente Codigo: " + res.getRespuestaValidarCliente().getIndicadorExito());
			LOGGER.info("Respuesta Valdiacion Datos Cliente Mensaje: " + res.getRespuestaValidarCliente().getDescription());	
		}
		catch(ValidarDatosClienteSIMAUTFaultMessage e){
			LOGGER.error("No es posible obtener el Servicio Valdiacion Datos Cliente",e);
			LOGGER.error("TIEMPO "+watch.time());
			logPerformance.error("[MOVIL "+validaDatosClientes.getMsisdn()+"]|[ValidarDatosClientesSimAut]|[TIEMPO "+watch.time()+"]");
			res.setRespuestaValidarCliente( new ValidarDatosClienteSIMAUTResponse() );
			res.setErrorValidarCliente( e.getFaultInfo() );
		}
		return res;	
	}
	@Override
	public RespuestaConsultaTitularDeudaSync consultaTitularDeudaSyncService(ReqConsultaTitularDeudaSyncVO parametrosVO){
		
        RespuestaConsultaTitularDeudaSync res = new RespuestaConsultaTitularDeudaSync();
		ParamConsultaTitularDeudaSync req     = new ParamConsultaTitularDeudaSync();
        NumeroTelefono numeroTelefono         = new NumeroTelefono();

		numeroTelefono.setANI(parametrosVO.getMsisdn());
		
        req.setRutSolicitante(parametrosVO.getRutSolicitante());
		req.setTipoServicioTo(parametrosVO.getTipoServicioTo());
		req.setTipoServicio(parametrosVO.getTipoServicio());
		req.setRutTitular(parametrosVO.getRutTitular());
		req.setModalidad(parametrosVO.getModalidad());
		req.getListaAnis().add(numeroTelefono);
		req.setIdd(parametrosVO.getIdd());

		LOGGER.info(parametrosVO.getRutSolicitante());
		LOGGER.info(parametrosVO.getTipoServicioTo());
		LOGGER.info(parametrosVO.getTipoServicio());
		LOGGER.info(parametrosVO.getRutTitular());
		LOGGER.info(parametrosVO.getModalidad());
		LOGGER.info(numeroTelefono);
		LOGGER.info(parametrosVO.getIdd());
		
		TimeWatch watch = TimeWatch.start();
		
		try {
			LOGGER.info("Inicio ConsultaTitularDeuda");
			
			SSGPConsultaTitularDeudaSyncService servicioConsultaDeuda = EntelServices.getConsultaTitularDeudaSyncService();
			
			res = servicioConsultaDeuda.getSSGPConsultaTitularDeudaSyncPort().consultaTitularDeudaSync(req);
			
			logPerformance.info("[MOVIL "+parametrosVO.getMsisdn()+"]|[EliminarNegocioHeader]|[CODIGO "+res.getErrorCode()+"]|[TIEMPO "+watch.time()+"]");
			
            LOGGER.info("Respuesta ConsultaTitularDeuda TIEMPO: "          + watch.time());
            LOGGER.info("Respuesta ConsultaTitularDeuda Codigo: "          + res.getErrorCode());
            LOGGER.info("Respuesta ConsultaTitularDeuda Descripcion: "     + res.getErrorDescription());
            LOGGER.info("Respuesta ConsultaTitularDeuda Fecha REspuesta: " + res.getFechaRespuesta());
            LOGGER.info("Respuesta ConsultaTitularDeuda ID Consulta OAP: " + res.getIdConsultaOAP());
            LOGGER.info("Respuesta ConsultaTitularDeuda ID Consulta GW: "  + res.getIdConsultaGW());
            LOGGER.info("Respuesta ConsultaTitularDeuda Respuesta ANI: "   + res.getRespuestasANI());
            
		} catch (SSGPConsultaTitularDeudaSyncException_Exception e) {
			LOGGER.error("No es posible obtener el Consulta Titular Deuda",e);
			LOGGER.error("TIEMPO "+watch.time());
			logPerformance.error("[MOVIL "+parametrosVO.getMsisdn()+"]|[ConsultaTitularDeudaSync]|[TIEMPO "+watch.time()+"]");
		}
		
		return res;
	}
	@Override
	public RespuestaConsultaFactPortacionSync consultaFactibilidadPortabilidad(ConsultaFactibilidadPortVO consultaFactibilidadPort){
    RespuestaConsultaFactPortacionSync res = new RespuestaConsultaFactPortacionSync();
    ParamConsultaFactPortacionSync req     = new ParamConsultaFactPortacionSync();
	
	req.setIdd(consultaFactibilidadPort.getIdd());
	req.setTipoServicio(consultaFactibilidadPort.getTipoServicio());
	req.setTipoServicioTo(consultaFactibilidadPort.getTipoServicioTo());
	
	NumeroTelefonoDetalle numeroTelefonoDetalle = new NumeroTelefonoDetalle();
	
	numeroTelefonoDetalle.setANI(consultaFactibilidadPort.getAni());
	numeroTelefonoDetalle.setIMEI(consultaFactibilidadPort.getImei());
	
	req.getListaAnis().add(numeroTelefonoDetalle);
	
	TimeWatch watch = TimeWatch.start();
	
	try{
		LOGGER.info("Inicio Consulta Factibilidad Portabilidad");
        SSGPConsultaFactPortacionSyncService port = EntelServices.getConsultaFactPortacion();
        res                                       = port.getSSGPConsultaFactPortacionSyncPort().consultaFactPortacionSync(req);
        logPerformance.info("[MOVIL "+consultaFactibilidadPort.getMsisdn()+"]|[RespuestaConsultaFactPortacionSync]|[CODIGO "+res.getErrorCode()+"]|[TIEMPO "+watch.time()+"]");
        LOGGER.info("Respuesta Consulta Factibilidad Portabilidad TIEMPO: "  + watch.time());
        LOGGER.info("Respuesta Consulta Factibilidad Portabilidad Codigo: "  + res.getErrorCode());
        LOGGER.info("Respuesta Consulta Factibilidad Portabilidad Mensaje: " + res.getErrorDescription());
	}
	catch(SSGPConsultaFactPortacionSyncException_Exception e){
		LOGGER.error("No es posible obtener el Servicio Consulta Factibilidad Portabilidad",e);
		LOGGER.error("TIEMPO "+watch.time());
		logPerformance.error("[MOVIL "+consultaFactibilidadPort.getMsisdn()+"]|[RespuestaConsultaFactPortacionSync]|[TIEMPO "+watch.time()+"]");
	}
	
	return res;	
}
	@Override
	public Operator obtieneCompania(String msisdnFull) throws EntelServicesLocatorException {
		Operator resCompania = new Operator();
		TimeWatch watch = TimeWatch.start();
		try {
			ValidaMsisdn port = EntelServices.validaCompania().getValidaMsisdnPort();
			ServiceUtils.setServiceTimeout(port);
			
			resCompania = port.validaMisdn(msisdnFull);
			msisdnFull = (null != msisdnFull) ? msisdnFull : "NULO";
			logPerformance.info("[MOVIL " + msisdnFull + "]|[validaMsisdnService.validaMisdn]|[TIEMPO " + watch.time() + "]");
			LOGGER.info("Respuesta Obtiene Compania TIEMPO: "+ watch.time());
			LOGGER.info("Respuesta Evaluacion Comercial Operador ID: " + resCompania.getOperatorId());
		} catch (ServiceException_Exception e) {
			LOGGER.error("Problema en servicio " + e.getMessage());
			msisdnFull = (null != msisdnFull) ? msisdnFull : "NULO";
			logPerformance.error("[MOVIL " + msisdnFull + "]|[validaMsisdnService.validaMisdn]|[TIEMPO " + watch.time() + "]|[ERROR" + e);
		} catch (Exception e) {
			LOGGER.error("Error al inicializar el Servicio " + ValidaMsisdnService.class, e);
			msisdnFull = (null != msisdnFull) ? msisdnFull : "NULO";
			logPerformance.error("[MOVIL " + msisdnFull + "]|[validaMsisdnService.validaMisdn]|[TIEMPO " + watch.time() + "]|[ERROR" + e);
			try {
				throw new EntelServicesLocatorException(e.getMessage());
			} catch (EntelServicesLocatorException e1) {
				LOGGER.error("No es posible crear la llamada al servicio ValidaMsisdnService: " + e);
				msisdnFull = (null != msisdnFull) ? msisdnFull : "NULO";
				logPerformance.error("[MOVIL " + msisdnFull + "]|[validaMsisdnService.validaMisdn]|[TIEMPO " + watch.time() + "]|[ERROR" + e);
			}
		}

		if (resCompania.getOperatorId() != null) {
			return resCompania;
		} else {
			return null;
		}
	}
	@Override
	public RespuestaGenerarCapVO generarCap(String ANI, String idd) {
		ParamSolicitaCAPAsync req = new ParamSolicitaCAPAsync();
		RespuestaSolicitaCAPAsync res = new RespuestaSolicitaCAPAsync();
		RespuestaGenerarCapVO respuesta = new RespuestaGenerarCapVO();
				//se genera el request del servicio segun parametros entregados
				req.setANI(ANI);
				req.setIdd(idd);
				TimeWatch watch = TimeWatch.start();
				try{			
					LOGGER.info("***** LLAMADO A GENERA CAP  *****");
					SSGPSolicitaCAPAsyncService solicitaCap = EntelServices.generaCap();
					res = solicitaCap.getSSGPSolicitaCAPAsyncPort().solicitaCAPAsync(req);
					logPerformance.info("[lineaClase :"+31+"]|[nombreMetodo : solicitaCAPAsync]|[ANI "+ ANI +"]|[SSGPSolicitaCAPAsyncService]|[TIEMPO "+watch.time()+"]|");
					LOGGER.info("[ANI : "+ ANI +"]|[SSGPSolicitaCAPAsyncService][TIEMPO "+watch.time()+"]");
					respuesta.setErrorCode(String.valueOf(res.getErrorCode()));
					respuesta.setErrorDescription(res.getErrorDescription());
					respuesta.setIdSolicitudGW(res.getIdSolicitudGW());
					LOGGER.info("Respuesta Genera CAP TIEMPO: "  + watch.time());
			        LOGGER.info("Respuesta Genera CAP Codigo: "  + res.getErrorCode());
			        LOGGER.info("Respuesta Genera CAP Mensaje: " + res.getErrorDescription());
				}catch (Exception e) {
					 LOGGER.error("No es posible obtener el servicio Generar Cap "
							    + e.getMessage());				   
					 logPerformance.error("[ANI : "+ ANI +"]|[SSGPSolicitaCAPAsyncService][TIEMPO "+watch.time()+"]"+e);
				}
				
	
		return respuesta;
	}
	@Override
	public ResGeneraCapSuscripcionVO generarCapSuscripcion(ReqGeneraCapSuscripcionVO datosCapS) {
		EnviarCapInternoSuscripcionRequest req = new EnviarCapInternoSuscripcionRequest();
		EnviarCapInternoSuscripcionResponse res = new EnviarCapInternoSuscripcionResponse();
		//Se reutiliza esta clase ya que devuelve el mismo request que donde se implementa
		ResGeneraCapSuscripcionVO respuesta = new ResGeneraCapSuscripcionVO();
		req.setCodPlataforma(datosCapS.getCodPlataforma());
		req.setNumeroMovil(Long.valueOf(datosCapS.getNumeroMovil()));
		TimeWatch watch = TimeWatch.start();
	
		try{			
			LOGGER.info("***** LLAMADO A GENERA CAP SUSCRIPCION  *****");
			EnviarCapInternoSuscripcion generaCapSuscripcion = EntelServices.generaCapSuscripcion();
			res = generaCapSuscripcion.getEnviarCapInternoSuscripcionPort().enviarCapInternoSuscripcion(req);
			logPerformance.info("[lineaClase :"+97+"]|[nombreMetodo : enviarCapInternoSuscripcion]|[Numero Movil : "+ datosCapS.getNumeroMovil()+"]|[EnviarCapInternoSuscripcion]|[TIEMPO "+watch.time()+"]|");
			LOGGER.info("[Numero Movil  : "+ datosCapS.getNumeroMovil() +"]|[EnviarCapInternoSuscripcion][TIEMPO "+watch.time()+"]");
			respuesta.setDescripcion(res.getDescription());
			respuesta.setIndicadorExito(res.getIndicadorExito());
			LOGGER.info("Respuesta Genera CAP Suscripcion TIEMPO: "  + watch.time());
	        LOGGER.info("Respuesta Genera CAP Suscripcion Codigo: "  + res.getIndicadorExito());
	        LOGGER.info("Respuesta Genera CAP Suscripcion Mensaje: " + res.getDescription());
		}catch (Exception e) {
			 LOGGER.error("No es posible obtener el servicio Genera Cap Suscripcion "
					    + e.getMessage());				   
			 logPerformance.error("[Numero Movil : "+ datosCapS.getNumeroMovil() +"]|[EnviarCapInternoSuscripcion][TIEMPO "+watch.time()+"]"+e);
		}
		return respuesta;
	}
	@Override
	public ResValidarDatosPortabilidadVO validarDatosPortabilidad(ReqValidarDatosPortabilidadVO datosPortabilidad) {
		ValidarDatosPortabilidadSIMAUTRequest req  = new ValidarDatosPortabilidadSIMAUTRequest();
		ValidarDatosPortabilidadSIMAUTResponse res = new ValidarDatosPortabilidadSIMAUTResponse();
		ResValidarDatosPortabilidadVO respuesta    = new ResValidarDatosPortabilidadVO();
		TimeWatch watch                            = TimeWatch.start();

		req.setCap(datosPortabilidad.getCap());
		req.setCodMercadoOrigen(Integer.valueOf(datosPortabilidad.getCodMercadoOrigen()));
		req.setCodPlataforma(datosPortabilidad.getCodPlataforma());
		req.setImei(datosPortabilidad.getImei());
		req.setNumeroMovil(datosPortabilidad.getNumeroMovil());
		req.setNumeroNegocio(datosPortabilidad.getNumeroNegocio());

		try{			
			LOGGER.info("***** LLAMADO A VALIDAR DATOS PORTABILIDAD  *****");
			ValidarDatosPortabilidadSIMAUT validarDatosPortabilidad = EntelServices.validarDatosPortabilidad();
			res                                                     = validarDatosPortabilidad.getValidarDatosPortabilidadSIMAUTPort().validarDatosPortabilidadSIMAUT(req);

			logPerformance.info("[lineaClase :"+66+"]|[nombreMetodo : ValidarDatosPortabilidadSIMAUT]|[Numero Movil "+ datosPortabilidad.getNumeroMovil() +"]|[ValidarDatosPortabilidadSIMAUT]|[TIEMPO "+watch.time()+"]|");
			LOGGER.info("[Numero Movil  : "+ datosPortabilidad.getNumeroMovil() +"]|[ValidarDatosPortabilidadSIMAUT][TIEMPO "+watch.time()+"]");

			respuesta.setDescripcion(res.getDescription());
			respuesta.setIndicadorExito(res.getIndicadorExito());
			LOGGER.info("Respuesta Validar Datos Portabilidad TIEMPO: "  + watch.time());
	        LOGGER.info("Respuesta Validar Datos Portabilidad Codigo: "  + res.getIndicadorExito());
	        LOGGER.info("Respuesta Validar Datos Portabilidad Mensaje: " + res.getDescription());

		}catch (Exception e) {
			LOGGER.error("No es posible obtener el servicio Validar Datos Portabilidad " + e.getMessage());				   
			logPerformance.error("[Numero Movil : "+ datosPortabilidad.getNumeroMovil() +"]|[ValidarDatosPortabilidadSIMAUT][TIEMPO "+watch.time()+"]"+e);
		}
		return respuesta;
	}
	
	@Override
	public String enviaEmail(String to, String from, String subject, String body, List<DataEmailVO> adjuntos) {
		
		String respuesta = "";
		TimeWatch watch = TimeWatch.start();
		LOGGER.info("to   : " + to);
		LOGGER.info("from   : " + from);
		LOGGER.info("subject           : " + subject);
		LOGGER.info("body           : " + body);
		for (DataEmailVO parametros : adjuntos) {
			LOGGER.info("DATA : data           : " + parametros.getData());
			LOGGER.info("DATA : nombre         : " + parametros.getNombre());
			LOGGER.info("DATA : tipo           : " + parametros.getTipo());
		}

		try {

			ListMailBeanLiteral listaEmail = new ListMailBeanLiteral();
			for (DataEmailVO parametros : adjuntos) {
				MailBean mail = new MailBean();

				mail.setData(parametros.getData().toString());
				mail.setName(parametros.getNombre().toString());
				mail.setType(parametros.getTipo().toString());
				listaEmail.getMailBean().add(mail);
			}
			EnviarMailService servicio = EntelServices.getRespuestaEmailInstanceService();
			respuesta = servicio.getEnviarMailSoapPort().enviarMail(to, from, subject, body, listaEmail);
			LOGGER.info("Tiempo:" + watch.time());
			LOGGER.info("Respuesta Email : " + respuesta);

		} catch (Exception e) {

			LOGGER.info("No se pudo hacer la llamada al servicio : EnviarMailService, Input:" + "[ to: " + to
					+ " ] [ from: " + from + "] [ subject: " + subject + "] " + "[ body: " + body + " ] "
					+ e.getMessage());
			respuesta = "no se realizo el envio del email";
		} finally {
			LOGGER.info("Fin consultando Disponibilidad");
		}

		return respuesta;

	}
	@Override
	public ResTicketSGAVO ticketSGA(TicketSGAVO ticket) {
		Cliente cliente = (Cliente) getSession().get("Cliente");
        ResTicketSGAVO respuesta       = new ResTicketSGAVO();
        CreaTicketSgaRequestType req   = new CreaTicketSgaRequestType();
        CrearTicketSgaResponseType res = new CrearTicketSgaResponseType();
		
		//se genera el request del servicio segun parametros entregados
		req.setCelularReclamado(ticket.getCelularReclamado());
		req.setMotivo(ticket.getMotivo());
		req.setMundo("Ventas Campanas Beneficios");
		req.setNroSolicitud("");
		req.setPlataforma("Portal MiEntel");
		req.setRequerimientoCliente(ticket.getRequerimientoCliente());
		req.setSolicitud("R");
		req.setSuboperacion("Venta Movil Ecommerce");
		req.setTipo("Solicitud");
		req.setValor(ticket.getValor());
		req.setWorkflow("1");

		TimeWatch watch = TimeWatch.start();
		
		try{			
			LOGGER.info("***** LLAMADO A TICKET SGA  *****");
            TicketSGAService ticketSGA = EntelServices.getTicketSGAService();
            res                        = ticketSGA.getTicketSGAServiceSOAPPortType().crearTicketSga(req);
            
			LOGGER.info("Respuesta del servicio "+ res.getMensaje() + " cod: " + res.getCodigo() );
			logPerformance.info("|[MOVIL "+cliente.getTelefono()+"]|"
					+ "[lineaClase :"+301+"]|"
							+ "[nombreMetodo : creaTicketSGA]|"
							+ "[msisdn "+ticket.getCelularReclamado()+"]|"
									+ "[TicketSGAService]|[TIEMPO "+watch.time()+"]|"
											+ "[codigoRespuesta :"+res.getCodigo()+"]|");
			LOGGER.info("[Numero Movil "+ticket.getCelularReclamado()+"]|[TicketSGAService][TIEMPO "+watch.time()+"]");
			
			respuesta.setCodigo(res.getCodigo());
			respuesta.setMensaje(res.getMensaje());		
			
		}catch (Exception e) {
			LOGGER.error("No es posible obtener el servicio TicketSGA " + e.getMessage());	
			e.printStackTrace();
			LOGGER.error("[MOVIL " + ticket.getCelularReclamado() + "]|[Ticket SGA]|[TIEMPO " + watch.time() + "]|" + e);
		}

		return respuesta;
	}
	private Map<String, Object> getSession() {
		Map<String, Object> attibutes = ActionContext.getContext().getSession();
		return attibutes;
	}
}
