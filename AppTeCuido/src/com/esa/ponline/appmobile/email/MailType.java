package com.esa.ponline.appmobile.email;

import java.io.Serializable;

/*
 * Type Para asignar los parametros que se deseen colocar en el correo
 */
public class MailType implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5554027721992358074L;
	//Resumen Plan
	private String nombrePlan ;
	private String minutosPlan;
	private String redesSocialesPlan;
	private String precioNormalPlan;
	private String precioConDescuento;
	private String textoPrecioConDescuento;
	//Direccion de Entrega
	private String nombreCliente;
	private String apellidoCliente;
	private String calleDespacho;
	private String numeroDespacho;
	private String comunaDespacho;
	private String regionDespacho;
	public String getNombrePlan() {
		return nombrePlan;
	}
	public void setNombrePlan(String nombrePlan) {
		this.nombrePlan = nombrePlan;
	}
	public String getMinutosPlan() {
		return minutosPlan;
	}
	public void setMinutosPlan(String minutosPlan) {
		this.minutosPlan = minutosPlan;
	}
	public String getRedesSocialesPlan() {
		return redesSocialesPlan;
	}
	public void setRedesSocialesPlan(String redesSocialesPlan) {
		this.redesSocialesPlan = redesSocialesPlan;
	}
	public String getPrecioNormalPlan() {
		return precioNormalPlan;
	}
	public void setPrecioNormalPlan(String precioNormalPlan) {
		this.precioNormalPlan = precioNormalPlan;
	}
	public String getPrecioConDescuento() {
		return precioConDescuento;
	}
	public void setPrecioConDescuento(String precioConDescuento) {
		this.precioConDescuento = precioConDescuento;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public String getApellidoCliente() {
		return apellidoCliente;
	}
	public void setApellidoCliente(String apellidoCliente) {
		this.apellidoCliente = apellidoCliente;
	}
	public String getCalleDespacho() {
		return calleDespacho;
	}
	public void setCalleDespacho(String calleDespacho) {
		this.calleDespacho = calleDespacho;
	}
	public String getNumeroDespacho() {
		return numeroDespacho;
	}
	public void setNumeroDespacho(String numeroDespacho) {
		this.numeroDespacho = numeroDespacho;
	}
	public String getComunaDespacho() {
		return comunaDespacho;
	}
	public void setComunaDespacho(String comunaDespacho) {
		this.comunaDespacho = comunaDespacho;
	}
	public String getRegionDespacho() {
		return regionDespacho;
	}
	public void setRegionDespacho(String regionDespacho) {
		this.regionDespacho = regionDespacho;
	}
	public String getTextoPrecioConDescuento() {
		return textoPrecioConDescuento;
	}
	public void setTextoPrecioConDescuento(String textoPrecioConDescuento) {
		this.textoPrecioConDescuento = textoPrecioConDescuento;
	}
	
}
