package com.esa.ponline.appmobile.interfaces;


import java.util.List;

import com.epcs.clientes.evaluacion.types.EvaluacionComercialNecResponseType;
import com.esa.crm.portabilidad.t.obtenercompaniamovil.Operator;
import com.esa.ponline.appmobile.web.bean.ConsultaFactibilidadPortVO;
import com.esa.ponline.appmobile.web.bean.DataEmailVO;
import com.esa.ponline.appmobile.web.bean.ReqConsultaTitularDeudaSyncVO;
import com.esa.ponline.appmobile.web.bean.ReqGeneraCapSuscripcionVO;
import com.esa.ponline.appmobile.web.bean.ReqValidarDatosPortabilidadVO;
import com.esa.ponline.appmobile.web.bean.ResGeneraCapSuscripcionVO;
import com.esa.ponline.appmobile.web.bean.ResTicketSGAVO;
import com.esa.ponline.appmobile.web.bean.ResValidarDatosClientesSIMMAUTVO;
import com.esa.ponline.appmobile.web.bean.ResValidarDatosPortabilidadVO;
import com.esa.ponline.appmobile.web.bean.RespuestaGenerarCapVO;
import com.esa.ponline.appmobile.web.bean.TicketSGAVO;
import com.esa.ponline.appmobile.web.bean.ValidaDatosClientesVO;
import com.esa.ponline.appmobile.ws.EntelServicesLocatorException;

import cl.iecisa.gateway.ws.integrator.services.ssgp_consultafactportacionsync.RespuestaConsultaFactPortacionSync;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultatitulardeudasync.RespuestaConsultaTitularDeudaSync;

public interface IServicesInterfaces {
	
	EvaluacionComercialNecResponseType obtenerEvaluacionComercial(String nRut, String dRut);
	ResValidarDatosClientesSIMMAUTVO validarDatosClientesSimAut(ValidaDatosClientesVO validaDatosClientes);
	RespuestaConsultaTitularDeudaSync consultaTitularDeudaSyncService(ReqConsultaTitularDeudaSyncVO parametrosVO);
	RespuestaConsultaFactPortacionSync consultaFactibilidadPortabilidad(ConsultaFactibilidadPortVO consultaFactibilidadPort);
	Operator obtieneCompania(String msisdnFull) throws EntelServicesLocatorException;
	RespuestaGenerarCapVO generarCap(String ANI, String idd);
	ResGeneraCapSuscripcionVO generarCapSuscripcion(ReqGeneraCapSuscripcionVO datosCapS);
	ResValidarDatosPortabilidadVO validarDatosPortabilidad(ReqValidarDatosPortabilidadVO datosPortabilidad);
	ResTicketSGAVO ticketSGA(TicketSGAVO ticket);
	String enviaEmail(String to, String from, String subject, String body, List<DataEmailVO> adjuntos);
}
