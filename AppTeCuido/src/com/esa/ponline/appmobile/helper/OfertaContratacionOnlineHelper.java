package com.esa.ponline.appmobile.helper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.esa.ponline.appmobile.web.actions.ContratacionAction;

public class OfertaContratacionOnlineHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4875225230868593954L;
	private static final Logger LOGGER      = Logger.getLogger(OfertaContratacionOnlineHelper.class);
	
	public static List<String> cargarTerminosyDebesSaber(String idTermino, HashMap<String, String> hashMapTerminos, String keyString){
		LOGGER.info("cargarTerminosyDebesSaber >>> Execute");
		LOGGER.info("Tamanio del debes Saber padre >>> "+ hashMapTerminos.size());
		Map<String, String> map = new TreeMap<String, String>(hashMapTerminos);
		map.entrySet();
		HashMap<String, String> hasgMapRespuesta = new HashMap<String, String>();
	    //Iteramos solo sobre los elementos key4
		LOGGER.info("Key para busqueda de terminos >>> "+ idTermino+keyString);
	    for (String key : map.keySet()) {
	        if(key.contains(idTermino+keyString)){
	        	hasgMapRespuesta.put(key,hashMapTerminos.get(key));
	        }
	    }
	    Map<String, String> map1 = new TreeMap<String, String>(hasgMapRespuesta);
	    map1.entrySet();
	    List<String> list = new ArrayList<String>(map1.values());
	    LOGGER.info("Tamanioo de TerminosyDebesSaber respuesta >>> "+ map1.size());
		return list;
	}
	
}
