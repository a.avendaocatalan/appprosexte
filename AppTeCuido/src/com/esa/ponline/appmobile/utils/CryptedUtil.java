package com.esa.ponline.appmobile.utils;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;

public class CryptedUtil {
	static Cipher cipher;
	
	 @SuppressWarnings("unused")
		private static String encriptar(String string) throws Exception {
			String keyString = "ptraficoexcedido";
			SecretKey secretKey = new SecretKeySpec(keyString.getBytes(),"AES");
			
			try {
				cipher = Cipher.getInstance("AES");
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (NoSuchPaddingException e) {
				e.printStackTrace();
			}
			
			
	    	return enCrypt(string, secretKey);
			
		}
	    
		public static String enCrypt(String plainText, SecretKey secretKey) {
			byte[] plainTextByte = plainText.getBytes();
			
			try {
				cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			} catch (InvalidKeyException e) {
				e.printStackTrace();
			}
			
			byte[] encryptedByte = null;
			try {
				encryptedByte = cipher.doFinal(plainTextByte);
			} catch (IllegalBlockSizeException e) {
				e.printStackTrace();
			} catch (BadPaddingException e) {
				e.printStackTrace();
			}
			
			char[] encryptedHex = Hex.encodeHex(encryptedByte);
			String encryptedText = new String(encryptedHex);
			return encryptedText;
		}
		
		public static String deCrypt(){
			return null;
		}
		
		public static void main(String[] args) throws Exception {
			String numero = encriptar("56942220186");
			String ip = encriptar("1.1.1.1");
			String schars = encriptar("33794");
			int ambiente = 0;
			String host;
			String url;
			switch (ambiente) {
			case 0: //PROD
				host = "http://appswls.entel.cl";
				url = "/empresas/autogestion/compraBolsas/inicio?msisdn="+ numero +"&ip="+ip +"&schar="+schars ;
				System.out.println(host.concat(url));
				break;
			case 1: //PRE
				host = "http://appswlspre.entel.cl";
				url = "/empresas/autogestion/compraBolsas/inicio?msisdn="+ numero +"&ip="+ip +"&schar="+schars ;
				System.out.println(host.concat(url));
				break;
			case 2: //LOCALHOST
				host = "http://localhost:7001";
				url = "/empresas/autogestion/compraBolsas/inicio?msisdn="+ numero +"&ip="+ip +"&schar="+schars ;
				System.out.println(host.concat(url));
				break;
			default:
				break;
			}
			
		}
}
