package com.esa.ponline.appmobile.config;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import com.opensymphony.xwork2.util.LocalizedTextUtil;
public class ActionGlobalMessagesListener implements ServletContextListener{
	private static final String DEFAULT_RESOURCE = "globalMessages";

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		URL[] urls;

		String resource_path = sce.getServletContext().getInitParameter("PATH");
		String resource_config = sce.getServletContext().getInitParameter("config");
		String resource_logs = sce.getServletContext().getInitParameter("logs");
		System.out.println("resource_path :" + resource_path);
		System.out.println("resource_config :" + resource_config);
		System.out.println("resource_logs :" + resource_logs);
		
		System.setProperty("rootMientelPath", resource_path + "/" + resource_logs);
		
		try {
			File file = new File(resource_path + "/" + resource_config);
			URL url = file.toURI().toURL();
			urls = new URL[] { url };
			
			ClassLoader cl = new URLClassLoader(urls);
			LocalizedTextUtil.setDelegatedClassLoader(cl);
			LocalizedTextUtil.addDefaultResourceBundle(DEFAULT_RESOURCE);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

	}

}
