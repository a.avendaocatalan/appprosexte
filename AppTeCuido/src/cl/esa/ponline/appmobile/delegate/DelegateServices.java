package cl.esa.ponline.appmobile.delegate;

import java.util.List;

import com.epcs.clientes.evaluacion.types.EvaluacionComercialNecResponseType;
import com.esa.crm.portabilidad.t.obtenercompaniamovil.Operator;
import com.esa.ponline.appmobile.web.bean.ConsultaFactibilidadPortVO;
import com.esa.ponline.appmobile.web.bean.DataEmailVO;
import com.esa.ponline.appmobile.web.bean.ReqConsultaTitularDeudaSyncVO;
import com.esa.ponline.appmobile.web.bean.ReqGeneraCapSuscripcionVO;
import com.esa.ponline.appmobile.web.bean.ReqValidarDatosPortabilidadVO;
import com.esa.ponline.appmobile.web.bean.ResGeneraCapSuscripcionVO;
import com.esa.ponline.appmobile.web.bean.ResTicketSGAVO;
import com.esa.ponline.appmobile.web.bean.ResValidarDatosClientesSIMMAUTVO;
import com.esa.ponline.appmobile.web.bean.ResValidarDatosPortabilidadVO;
import com.esa.ponline.appmobile.web.bean.RespuestaGenerarCapVO;
import com.esa.ponline.appmobile.web.bean.TicketSGAVO;
import com.esa.ponline.appmobile.web.bean.ValidaDatosClientesVO;
import com.esa.ponline.appmobile.ws.FactoryWSDAO;

import cl.iecisa.gateway.ws.integrator.services.ssgp_consultafactportacionsync.RespuestaConsultaFactPortacionSync;
import cl.iecisa.gateway.ws.integrator.services.ssgp_consultatitulardeudasync.RespuestaConsultaTitularDeudaSync;

public class DelegateServices {

	public EvaluacionComercialNecResponseType obtenerEvaluacionComercial(String nRut, String dRut)throws Exception{
		return FactoryWSDAO.getServices().obtenerEvaluacionComercial(nRut, dRut);
	}	
	public ResValidarDatosClientesSIMMAUTVO validarDatosClientesSimAut(ValidaDatosClientesVO validaDatosClientes)throws Exception{
		return FactoryWSDAO.getServices().validarDatosClientesSimAut(validaDatosClientes);
	}	
	public RespuestaConsultaTitularDeudaSync consultaTitularDeudaSyncService(ReqConsultaTitularDeudaSyncVO parametrosVO)throws Exception{
		return FactoryWSDAO.getServices().consultaTitularDeudaSyncService(parametrosVO);
	}	
	public RespuestaConsultaFactPortacionSync consultaFactibilidadPortabilidad(ConsultaFactibilidadPortVO consultaFactibilidadPort)throws Exception{
		return FactoryWSDAO.getServices().consultaFactibilidadPortabilidad(consultaFactibilidadPort);
	}
	public Operator obtieneCompania(String msisdnFull) throws Exception{
		return FactoryWSDAO.getServices().obtieneCompania(msisdnFull);
	}
	public RespuestaGenerarCapVO generarCap(String ANI, String idd) throws Exception{
		return FactoryWSDAO.getServices().generarCap(ANI, idd);
	}
	public ResGeneraCapSuscripcionVO generarCapSuscripcion(ReqGeneraCapSuscripcionVO datosCapS) throws Exception{
		return FactoryWSDAO.getServices().generarCapSuscripcion(datosCapS);
	}	
	public ResValidarDatosPortabilidadVO validarDatosPortabilidad(ReqValidarDatosPortabilidadVO datosPortabilidad) throws Exception{
		return FactoryWSDAO.getServices().validarDatosPortabilidad(datosPortabilidad);
	}
	public String enviaEmail(String to, String from, String subject, String body, List<DataEmailVO> adjuntos) throws Exception{
		return FactoryWSDAO.getServices().enviaEmail(to, from, subject, body, adjuntos);
	}
	public ResTicketSGAVO ticketSGA(TicketSGAVO ticket) throws Exception{
		return FactoryWSDAO.getServices().ticketSGA(ticket);
	}
	
	
}